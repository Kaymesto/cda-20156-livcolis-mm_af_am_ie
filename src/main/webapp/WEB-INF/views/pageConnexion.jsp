<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
<title>Connexion</title>
</head>
<body>
<h2>Espace Connexion</h2>



<form class="formAuth" action="checkAuth" method="post">
<p class="paraConnect">Veuillez-vous identifier ci-dessous : </p> <br>
<c:if test="${errorConnection == true}"><p class="errorConnexion">Identifiants/mot de passe incorrects, veuillez r�essayer </p></c:if>
<label for="login">Login :</label> <br>
<input name="login" type="text" required /> <br>


<label for="mdp">Mot de passe :</label> <br>
<input name="mdp" type="password" required /> <br>

<div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Valider" />
<input class="cancel" name="cancel" type="button" value="Annuler" onclick="window.location.href='${pageContext.request.contextPath}'" />
</div>
</form>

</body>
</html>