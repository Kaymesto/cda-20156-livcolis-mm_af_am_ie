<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>formulaire creation point relais</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
<h2>Creation point relais</h2>
<section>
<form class="formCreaPtRelais" action="${pageContext.request.contextPath}/enregistrementPointRelais" method="post" >
<p></p>
<p></p>
<label for="nom">Nom du point relais :</label>
<input type="text" name="nom" required/>
<p></p>
<p></p>
<label for="ouvertureMatin">Ouverture le matin de :</label>
<input type="number" name="ouvertureMatin"  required/>
<label for="fermetureMatin">h � </label>
<input type="number" name="fermetureMatin" required/>
<label for="fermetureMatin">h</label>
<p></p>
<label for="ouverturePM">Ouverture l'apr�s midi de :</label>
<input type="number" name="ouverturePM" required/>
<label for="fermeturePM">h � </label>
<input type="number" name="fermeturePM" required/>
<label for="fermeturePM">h</label>
<p></p>
<p></p>
<p  class= "paraConnect" style='text-align:center'  >Adresse </p>
<p></p>
<p></p>
<label for="numeroRue">num�ro de rue :</label>
<input type="number" name="numeroRue" required/>
<p></p>
<p></p>
<label for="rue">rue :</label>
<input type="text" name="rue" required/>
<p></p>
<p></p>
<label for="cp">code postal :</label>
<input type="number" name="cp" pattern="[0-9]{5}"required/>
<p></p>
<p></p>
<label for="ville">ville :</label>
<input type="text" name="ville" required/>
<p></p>
<p></p>
<label for="pays">pays :</label>
<input type="text" name="pays" required/>
<p></p>
<p></p>
<label for="tel">Num�ro de t�l�phone :</label>
<input type="tel" id="tel" name="tel"
       pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}"
       required>

<small style="color:white"> Format: 0320000000</small>
<p></p>
<input class="valid" type="submit" name="bouton" value="Envoyer"/>

<input class="valid" type="button" value="Retour" 
			onclick="window.location.href='${pageContext.request.contextPath}/retourAccueilPointRelais'">
  <footer>
  <input class="valid" type="button" value="Espace menbre" onclick="window.location.href='${pageContext.request.contextPath}/espaceMembre'"/>
</footer>
</form>
</section>
</body>
</html>