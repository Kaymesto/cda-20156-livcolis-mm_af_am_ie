 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>
<h2>Chercher un client</h2>

<form class="formInscription" action="findByNom" method="post">
<br>
 NOM:<input type="text" name="nom" required />
<br>
 PRENOM:<input type="text" name="prenom" required />
 <br>
 
 <div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Rechercher" />
<input class="cancel" name="cancel" type="button" value="Accueil"  onclick="window.location.href='${pageContext.request.contextPath}/listeClients/'" />
</div>
 </form>
 
</body>
</html>