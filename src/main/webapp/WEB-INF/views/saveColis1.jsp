<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Enregistrement colis</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
<h2>Enregistrement colis</h2>
<table class="redTable">
<tr>
<th>ID </th>
<th>Nom </th>
<th>Mail </th>

</tr>
<c:forEach items="${listeClient.content}" var="client">
<tr>
<td> ${client.idClient}</td>
<td> ${client.nom} </td>
<td> ${client.mail} </td>

</tr>
</c:forEach>
</table>
<div id=nav class="container">
<ul  class="nav nav-pills align-content-space-around">

<c:forEach items="${pages}" var="p">
<a href="${pageContext.request.contextPath}/saveColis?page=${p}">${p+1}</a>
</c:forEach>

</ul>
</div>
<form class="formAuth" action="saveColis1" method="post">

<p class="paraConnect">Informations de l'expediteur</p> 
<label for="mailExp">E-mail :</label> <br>
<input name="mailExp" type="email" required /> <br>

<c:if test="${expExist == false }"><p class="errorConnexion">Expediteur inconnu, veuillez r�essayer ou le cr�er <a href="${pageContext.request.contextPath}/ajouterClient" >ici</a> </p></c:if>
<br>
<p class="paraConnect">Informations du destinataire</p>
<label for="mailDest">E-mail :</label> <br>
<input name="mailDest" type="email" required /> <br>

<c:if test="${destExist == false }"><p class="errorConnexion">Destinataire inconnu, veuillez r�essayer ou le cr�er <a href="${pageContext.request.contextPath}/ajouterClient" >ici</a> </p></c:if>
<br>
<p class="paraConnect">Informations du colis</p>
<label for="poids">Poids :</label> <br>
<input name="poids" type="text" required /> <br>
<label for="prix">Prix :</label> <br>
<input name="prix" type="text" /> <br>

<div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Suivant >>" />
<input class="cancel" name="cancel" type="button" value="Annuler" onclick="window.location.href='${pageContext.request.contextPath}/espaceMembre'" />
</div>
</form>


</body>
</html>