<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
<h2>Formulaire de modification client</h2>

<form class="formAuth" action="${pageContext.request.contextPath}/formModifierClient" method="post">

<input type="hidden" name="idClient" value="${client.idClient}" />

Nom : <input type="text" name="nom" value="${client.nom }" />
<br>
Prenom : <input type="text" name="prenom" value="${client.prenom }" />
<br>
Mail : <input type="email" name="mail" value="${client.mail }" />
<br>
Telephone : <input type="text" name="telephone" value="${client.telephone }" />
<br>
<div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Valider"    />
<input class="cancel" name="cancel" type="button" value="Accueil"  onclick="window.location.href='${pageContext.request.contextPath}/listeClients/'" />
</div>
</form>

		 
</body>
</html>