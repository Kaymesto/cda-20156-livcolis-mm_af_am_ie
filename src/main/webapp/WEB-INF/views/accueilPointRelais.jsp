<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<html>
<head>
<title>Accueil point relais</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>

</head>
<body>
	<h2>Accueil point relais</h2>
	
	<table class="redTable" id='tab'>
	
		<thead>
			<tr>
				<th><a
					href="${pageContext.request.contextPath}/pointRelaisTrierNomRecherche">Nom</a></th>
				<th><a>Adresse</a></th>
				<th><a>horaire d'ouverture</a></th>
				<th><a>telephone</a></th>
				<th>Supprimer</th>
				<th>Modifier</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
			</tr>
		</tfoot>
		<c:forEach items="${listePtRelais}" var="ptRelais">
			<tbody>
				<tr>
					<td><c:out value="${ptRelais.nomPtRelais}" /></td>
					<td><c:out
							value="${ptRelais.adresse.numero} ${ptRelais.adresse.rue}  ${ptRelais.adresse.codePostal} 
${ptRelais.adresse.ville} ${ptRelais.adresse.pays}" /></td>
					<td><c:out value="${ptRelais.horaireOuverture}" /></td>
					<td><c:out value="${ptRelais.telephone}" /></td>
					<td><a    onclick="return confirm('&Ecirc;tes-vous s�r de vouloir supprimer le point relais ?')" href="${pageContext.request.contextPath}/PointRelaisSupp/${ptRelais['idPointRelais']}"><img src="${pageContext.request.contextPath}/resources/images/icons8-supprimer-100.png" width="32" height="32"/></a>
					</td>
					<td><a
						href="${pageContext.request.contextPath}/pointRelaisModif/${ptRelais['idPointRelais']}"><img src="${pageContext.request.contextPath}/resources/images/icons8-modifier-96.png" width="32" height="32"/>
					</a></td>
		</c:forEach>

	</table>
	<nav>
	<div class="groupBoutons">
		<input class="valid" type="button" value="Creer point relais"
			onclick="window.location.href='${pageContext.request.contextPath}/pointRelaisCreation'" />
		<input class="valid" type="button" value="Rechercher"
			onclick="window.location.href='${pageContext.request.contextPath}/pointRelaisRecherche'" />
		<p></p>
		<p></p>
		</div>
		<div class="groupBoutons">
		<input class="valid" type="button" value="Espace membre "
			onclick="window.location.href='${pageContext.request.contextPath}/espaceMembre'" />
		<input class="valid" type="button" value="Deconnexion "
			onclick="window.location.href='${pageContext.request.contextPath}/deconexion'" />
			<p></p>
			<p></p>
</div>
</nav>

	
</body>
</html>