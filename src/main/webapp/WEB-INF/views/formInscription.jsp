<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Inscription</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>

<h2>Cr�ation de compte</h2>


<form class="formInscription" action="saveTransporteur" method="post">
<c:if test="${error == true }"><p class="paraConnect" style="color:red">Vos mots de passe et/ou e-mails ne concordent pas, veuillez v�rifier !</p></c:if>
<label for="nom">Nom :</label> <br>
<input name="nom" type="text" value="${transporteur.nom }" required /> <br>


<label for="prenom">Pr�nom :</label> <br>
<input name="prenom" type="text" value="${transporteur.prenom }" required /> <br>

<c:if test="${mailExist == true }"><p class="paraConnect" style="color:red">Cette adresse e-mail est d�j� utilis�e !</p></c:if>
<label for="mail">Adresse e-mail :</label> <br>
<input name="mail" type="email" value="${transporteur.mail }" pattern="[A-Za-z0-9\.]{5,30}@[A-Za-z]{3,10}\.[a-zA-Z]{2,5}" required /> <br>


<label for="mail2">Confirmation e-mail :</label> <br>
<input name="mail2" type="email" pattern="[A-Za-z0-9\.]{5,30}@[A-Za-z]{3,10}\.[a-zA-Z]{2,5}" required /> <br>

<label for="tel">Num�ro de t�l�phone :</label> <br>
<input name="tel" type="text" value="${transporteur.telephone }" pattern="[0-9]{10}" required /> <br>

<c:if test="${loginExist == true }"><p class="paraConnect" style="color:red">Cet identifiant est d�j� utilis�e !</p></c:if>
<label for="login">Identifiant :</label> <br>
<input name="login" type="text" value="${transporteur.auth.login }" pattern="[A-Za-z0-9]{5,30}" required /> <br>


<label for="mdp">Mot de passe :</label> <br>
<input name="mdp" type="password" value="${transporteur.auth.password }" pattern="[A-Za-z0-9]{8,}" required /> <br>

<label for="mdp2">Confirmation mdp :</label> <br>
<input name="mdp2" type="password" pattern="[A-Za-z0-9]{8,}" required /> <br>

<div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Valider" />
<input class="cancel" name="cancel" type="button" value="Annuler" onclick="window.location.href='${pageContext.request.contextPath}'" />
</div>
</form>
</body>
</html>