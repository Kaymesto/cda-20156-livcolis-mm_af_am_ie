<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>
Que voulez-vous modifier?

<form action="${pageContext.request.contextPath}/formModifierColis" method="post">
	<table class="redTable">
<tr> <td>Identifiant : <input type="text" name="idColis" value="${colis.idColis }" readonly="readonly" /></td></tr>
<tr> <td>Poids : <input type="number"  min="0" pattern="0.00" name="poidsColis" value="${colis.poids }" required="required"/></td></tr>
<tr><td> Prix : <input type="number"  min="0" name="prixColis" value="${colis.prix }" required="required"/></td></tr>
<tr> <td>code : <input type="text" name="codeBarreColis" value="${colis.codeBarre }" readonly="readonly"/></td></tr>
<tr> <td>Livré : <input type="text" name="etatColis" value="${colis.archive }" readonly="readonly"/> </td></tr>
<tr> <td>Etape point relais : <input type="text" name="etapeRelaisColis" value="${colis.etapePointRelais}-" readonly="readonly"/></td></tr>
<tr> <td>Mail expéditeur : <input type="text" name="mailExpedColis" value="${colis.idDeExpediteur.mail }" /></td></tr>
<tr> <td>Mail destinataire : <input type="text" name="mailDestColis" value="${colis.idDuDestinataire.mail }" /></td></tr>
<br>
</table>
<a href="${pageContext.request.contextPath}/AccueilModificationColis">Retourner à l'accueil</a>

<button type="submit">Valider</button>
</form>
		 
</body>
</html>