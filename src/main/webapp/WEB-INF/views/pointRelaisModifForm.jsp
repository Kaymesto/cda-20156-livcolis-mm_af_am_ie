<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
<title>modification du point relais</title>
</head>
<body>
<h2>Modification du point relais</h2>
<section>
<form action="${pageContext.request.contextPath}/enregistrementModifPointRelais" method="post" >
<p></p>
<p></p>
<label for="nom">Nom du point relais :</label>
<input type="text" name="nom" value="${pointRelais.nomPtRelais}" required/>
<p></p>
<p></p>
<label for="ouvertureMatin">Ouverture le matin de :</label>
<input type="number" name="ouvertureMatin" value="${matin}" required/>
<label for="fermetureMatin">h � </label>
<input type="number" name="fermetureMatin" value="${finAM}" required/>
<label for="fermetureMatin">h</label>
<p></p>
<label for="ouverturePM">Ouverture l'apr�s midi :</label>
<input type="number" name="ouverturePM" value="${debutPM}" required/>
<label for="fermeturePM">h � </label>
<input type="number" name="fermeturePM" value="${fermeture}" " required/>
<label for="fermeturePM">h</label>
<p></p>
<p></p>
<p  class= "paraConnect" style='text-align:center'  >Adresse </p>
<p></p>
<p></p>
<label for="numeroRue">num�ro de rue :</label>
<input type="number" name="numeroRue" value="${pointRelais.adresse.numero}" required/>
<p></p>
<p></p>
<label for="rue">rue :</label>
<input type="text" name="rue" value="${pointRelais.adresse.rue}" required/>
<p></p>
<p></p>
<label for="cp">code postal :</label>
<input type="number" name="cp" value="${pointRelais.adresse.codePostal}" pattern="[0-9]{5}"required/>
<p></p>
<p></p>
<label for="ville">ville :</label>
<input type="text" name="ville" value="${pointRelais.adresse.ville}" required/>
<p></p>
<p></p>
<label for="pays">pays :</label>
<input type="text" name="pays" value="${pointRelais.adresse.pays}" required/>
<p></p>
<p></p>
<label for="tel">Num�ro de t�l�phone :</label>
<input type="tel" id="tel" name="tel" value="${pointRelais.telephone}"
       pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}"
       required>

<small style="color:white"> Format: 0320000000</small>
<p></p>
<input class="valid" type="submit" name="bouton" value="Envoyer"/>

<input class="valid" type="button" value="Retour" 
			onclick="window.location.href='${pageContext.request.contextPath}/retourAccueilPointRelais'">
  <input type="hidden" name="id" id="id" value="${pointRelais.idPointRelais}"/>
  <footer>
  <input class="valid" type="button" value="Espace menbre" onclick="window.location.href='${pageContext.request.contextPath}/espaceMembre'"/>
</footer>
</form>
</section>
</body>
</html>