<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>
<h2>Liste de clients trouvés</h2>

<TABLE BORDER="1"  class="redTable">
    <TR>
        <TH>Numéro</TH>
        <TH>Nom</TH>
        <TH>Prénom</TH>
        <TH>Mail</TH>
        <TH>Téléphone</TH>
        <TH>Supprimer</TH>
        <TH>Modifier</TH>
    </TR>
    <c:forEach items="${ListeClie}" var="client">
            <tr>
                <td><c:out value="${client.idClient}" /></td>
                <td><c:out value="${client.nom}" /></td>
                <td><c:out value="${client.prenom}" /></td>
                <td><c:out value="${client.mail}" /></td>
                <td><c:out value="${client.telephone}" /></td>
                
                <td><a onclick="return confirm('Etes-vous sur de vouloir supprimer le client ?')"  href="${pageContext.request.contextPath}/supprimerClient/${client.idClient }"><img src="${pageContext.request.contextPath}/resources/images/icons8-supprimer-100.png" width="32" height="32"/></a></td>
                <td><a href="${pageContext.request.contextPath}/modifier/${client.idClient }"><img src="${pageContext.request.contextPath}/resources/images/icons8-modifier-96.png"width="32" height="32"/></a></td>
               
                
            </tr>
            </c:forEach>

</TABLE>
<br>
<input class="cancelCenter" name="myCentralButton" type="button" value="Accueil"   onclick="window.location.href='${pageContext.request.contextPath}/listeClients/'" />

</body>
</html>