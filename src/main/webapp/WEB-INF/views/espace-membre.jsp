<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

<title>Espace Membre</title>
</head>
<body>
<h1 class="mainTitle">Bienvenue, <c:out value="${trans.prenom} " /></h1>

<h2><a href="${pageContext.request.contextPath}/saveColis">Enregistrer colis</a></h2>

<h2><a href="${pageContext.request.contextPath}/listeClients">Gestion Clients</a></h2>

<h2><a href="${pageContext.request.contextPath}/pointRelaisAccueil">Gestion Relais</a></h2>

<h2><a href="${pageContext.request.contextPath}/AccueilModificationColis">Gestion Colis</a></h2>

<h2><a href="${pageContext.request.contextPath}">Deconnexion</a></h2>
</body>
</html>