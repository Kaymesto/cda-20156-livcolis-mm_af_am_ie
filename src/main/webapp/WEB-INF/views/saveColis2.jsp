<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Enregistrement colis</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
<script type="text/Javascript" src="${pageContext.request.contextPath}/resources/js/script.js"></script>
</head>
<body>
<h2>Enregistrement itin�aire</h2>

<p class="paraConnect">Liste des points relais</p>

<table class="redTable">
<tr>
<th>ID </th>
<th>Nom </th>
<th>Adresse </th>
<th>Horaires </th>
<th>T�l�phone </th>
</tr>
<c:forEach items="${listeRelais.content}" var="relais">
<tr>
<td> ${relais.idPointRelais}</td>
<td> ${relais.nomPtRelais} </td>
<td> ${relais.adresse} </td>
<td> ${relais.horaireOuverture} </td>
<td> ${relais.telephone}</td>
</tr>
</c:forEach>
</table>
<div id=nav class="container">
<ul  class="nav nav-pills align-content-space-around">

<c:forEach items="${pages}" var="p">
<a href="${pageContext.request.contextPath}/saveColis1?page=${p}&idColis=${colis.idColis}">${p+1}</a>
</c:forEach>

</ul>
</div>

<form class="formDynamik" name="formulaireDynamique" class="formAuth" action="saveColis2" method="post">

<p class='paraConnect'> Informations du point relais de d�part</p> 
<input name="idColis" type="hidden" value="${colis.idColis}"  />


<label for="idDepart">ID Point Relais:</label> <br>
<input name="idDepart" type="text" required /> <br>

<input class="home" type="button" onclick="ajout(this);"  value="ajouter un point relais"/>

<p class='paraConnect'>Informations du point relais d'arriv�e</p>
<label for="idArrivee">ID Point Relais:</label> <br>
<input name="idArrivee" type="text" required /> <br>




<div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Enregistrer" />
<input class="cancel" name="cancel" type="button" value="Annuler" />

</div>
</form>

</body>
</html>