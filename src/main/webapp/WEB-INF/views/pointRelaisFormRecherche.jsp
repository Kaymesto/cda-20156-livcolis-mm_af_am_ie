<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Recherhche</title>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
	<h2>Rechercher par points relais</h2>
	<form action="${pageContext.request.contextPath}/recherchePointRelais"
		method="post">
		<label for="mySearch">Rechercher par :</label>
		<div>
			<p></p>
			<p></p>
			<label for="nom">Nom du PointRelais :</label> <input type="search"
				id="nom" name="nom">
			<p></p>
			<p></p>
			<label for="tel">Telephone :</label> <input type="search" id="tel"
				name="tel">
			<p></p>
			<p></p>
			<label for="ville">Ville :</label> <input type="search" id="ville"
				name="ville">
			<p></p>
			<p></p>
			<label for="cp">Code postal :</label> <input type="search" id="cp"
				name="cp">
			<p></p>
			<p></p>
			<input type="submit" class="valid" value="valider" /> <span
				class="validity"></span>
		</div>
		<input class="valid" type="button" value="Retour"
			onclick="window.location.href='${pageContext.request.contextPath}/retourAccueilPointRelais'"></input>
		<input class="valid" type="button" value="Espace membre"
			onclick="window.location.href='${pageContext.request.contextPath}/espaceMembre'"></input>
	</form>
</body>
</html>