<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"

    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>
<h2>Liste de vos colis</h2>
<TABLE BORDER="1" class="redTable">
    <TR>
        <TH>Identifiant</TH>
        <TH>Prix</TH>
        <TH>Poids</TH>
        <TH>Numéro suivi</TH>
        <TH>Livré</TH>
        <TH>mail expéditeur</TH>
        <TH>Mail destinataire</TH>
        <TH>Etape Point Relais</TH>
        <TH>Passer le colis au point relais suivant</TH>
        <TH>Modifier</TH>
        <TH>Supprimer</TH>
        
    </TR>
    <c:forEach items="${listeColis }" var="colis">
            <tr>
                <td><c:out value="${colis.idColis}" /></td>
                <td><c:out value="${colis.prix}" /></td>
                <td><c:out value="${colis.poids}" /></td>
                <td><a href="${pageContext.request.contextPath}/affichageSuiviColis/${colis.codeBarre}">${colis.codeBarre}</a></td>
                <td><c:if test="${colis.archive == false}">Non</c:if><c:if test="${colis.archive == true}">Oui</c:if></td>
                <td><c:out value="${colis.idDeExpediteur.mail}" /></td>
                <td><c:out value="${colis.idDuDestinataire.mail}" /></td>
                <td><c:out value="${colis.etapePointRelais} / ${tailleColis}" /></td>
                <td><a href="${pageContext.request.contextPath}/etapeSuivanteColis/${colis.idColis}">Passer le colis au Point Relais Suivant</a></td>
                <td><a href="${pageContext.request.contextPath}/modifierColis/${colis.idColis}"><img src="${pageContext.request.contextPath}/resources/images/icons8-modifier-96.png" width="32" height="32"/></a></td>
                <td><a  onclick="return confirm('Etes-vous sur de vouloir supprimer le colis ?')" href="${pageContext.request.contextPath}/supprimerColis/${colis.idColis }"><img src="${pageContext.request.contextPath}/resources/images/icons8-supprimer-100.png" width="32" height="32"/></a></td>
            </tr>
    </c:forEach>
</TABLE>
<div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Rechercher Colis" onclick="window.location.href='${pageContext.request.contextPath}/rechercherColis'"     />
<input class="cancel" name="cancel" type="button" value="Accueil"  onclick="window.location.href='${pageContext.request.contextPath}/espaceMembre'" />
</div>

</body>




</html>