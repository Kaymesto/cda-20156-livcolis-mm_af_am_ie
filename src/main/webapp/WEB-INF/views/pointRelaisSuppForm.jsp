<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Confirmation de suppression</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
<h2>Suppression d un colis</h2>
<section>
<form action="${pageContext.request.contextPath}/PointRelaisSupp" method="post" >
<label for="nom">Nom du point relais :</label>
<input type="text" value="${pointRelais.nomPtRelais}" name="nom" required/>
<p></p>
<p></p>
<label for="ouvertureMatin">Ouverture le matin de :</label>
<input type="text" name="ouvertureMatin" value="${matin}" required/>
<label for="fermetureMatin">h � </label>
<input type="text" name="fermetureMatin" value="${finAM}" required/>
<label for="fermetureMatin">h</label>
<p></p>
<label for="ouverturePM">Ouverture l'apr�s midi :</label>
<input type="text" name="ouverturePM" value="${debutPM}" required/>
<label for="fermeturePM">h � </label>
<input type="text" name="fermeturePM" value="${fermeture}" required/>
<label for="fermeturePM">h</label>
<p></p>
<p></p>
<label for="adresse">Adresse :</label>
<p></p>
<p></p>
<label for="numeroRue">num�ro de rue :</label>
<input type="text" name="numeroRue" value="${pointRelais.adresse.numero}" required/>
<p></p>
<p></p>
<label for="rue">rue :</label>
<input type="text" name="rue" value="${pointRelais.adresse.rue}" required/>
<p></p>
<p></p>
<label for="cp">code postal :</label>
<input type="text" name="cp" value="${pointRelais.adresse.codePostal}" required/>
<p></p>
<p></p>
<label for="ville">ville :</label>
<input type="text" name="ville" value="${pointRelais.adresse.ville}" required/>
<p></p>
<p></p>
<label for="pays">pays :</label>
<input type="text" name="pays" value="${pointRelais.adresse.pays}" required/>
<p></p>
<p></p>
<label for="tel">Num�ro de t�l�phone :</label>
<input type="tel" id="tel" name="tel" value="${pointRelais.telephone}"
       pattern="[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}[0-9]{2}"
       required>

<small>Format: 0320000000</small>
<p></p>
<button type="submit" name="bouton" value="Envoyer">Supprimer</button>

<input type="hidden" name="id" id="id" value="${pointRelais.idPointRelais}"/>

 <button type="button" value="Retour" onclick="window.location.href='${pageContext.request.contextPath}/retourAccueilPointRelais'">retour</button>
 <h2> <a href="${pageContext.request.contextPath}/espaceMembre">Retour vers l espace membre</a></h2>
</form>
</section>
</body>
</html>