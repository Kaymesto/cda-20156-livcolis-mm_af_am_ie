 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />

</head>
<body>
<h2>Recherche de colis</h2>
<form class="formInscription" action="rechercherColis" method="post">
 Identifiant : <input type="text" name="idColis" /><br>
 Mail Expéditeur : <input type="text" name="mailExp" /> <br>
 Mail Destinataire : <input type="text" name="mailDest" /> <br>
<div class="groupBoutons">
<input class="valid" name="valid" type="submit" value="Rechercher"    />
<input class="cancel" name="cancel" type="button" value="Accueil"  onclick="window.location.href='${pageContext.request.contextPath}/AccueilModificationColis'" />
</div>
</form>

</body>
</html>