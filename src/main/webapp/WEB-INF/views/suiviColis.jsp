<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Suivi de colis</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css" />
</head>
<body>
<h2>Suivi de votre colis</h2>

<p class="paraConnect"> Colis n� ${colis.codeBarre} envoy� par ${colis.idDeExpediteur.nom } ${colis.idDeExpediteur.prenom }</p>
<br>

<c:forEach items="${colis.listeJournalDeBord}" var="journal">

<c:if test="${empty journal.dateArrivee}">
<p class="paraConnect" style='color: grey'><img src="${pageContext.request.contextPath}/resources/images/remove.png"/> <c:out value="${journal.dateArrivee  }  Arriv�e au point relais ${journal.pointRelais.nomPtRelais } � ${journal.pointRelais.adresse.ville } (${journal.pointRelais.adresse.codePostal })"></c:out></p>
</c:if>
<c:if test="${not empty journal.dateArrivee}">
<p class="paraConnect" style='color: #34c924'><img src="${pageContext.request.contextPath}/resources/images/check.png"/> <c:out value="${journal.dateArrivee  }   Arriv�e au point relais ${journal.pointRelais.nomPtRelais } � ${journal.pointRelais.adresse.ville } (${journal.pointRelais.adresse.codePostal })"></c:out></p>
</c:if>


<br>
</c:forEach>

<div class="banConf">
<a class="home" href="${pageContext.request.contextPath}" >Retour � l'accueil</a>
</div>
</body>
</html>