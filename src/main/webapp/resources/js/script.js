function ajout(element){
        var formulaire = window.document.formulaireDynamique;
        // On clone le bouton d'ajout
        var ajout = element.cloneNode(true);
        // Crée 4 nouvels élément de type "input"
        var champ1 = document.createElement("input");

        // Les valeurs encodée dans le formulaire seront stockées dans un tableau
        champ1.name = "champs1[]";
        champ1.type = "text";
		champ1.style.width="500px";

        var sup = document.createElement("input");
        sup.value = "supprimer point relais";
        sup.type = "button";
		sup.classList.add("home");
        // Ajout de l'événement onclick
        sup.onclick = function onclick(event)
        {suppression(this);};

        // On crée un nouvel élément de type "p" et on insère le champ a l'intérieur.
        var bloc1 = document.createElement("p");
        bloc1.appendChild(champ1);
        formulaire.insertBefore(ajout, element);
        formulaire.insertBefore(sup, element);
        formulaire.insertBefore(bloc1, element);
       
      }

      function suppression(element){
        var formulaire = window.document.formulaireDynamique;

        // Supprime le bouton d'ajout
        formulaire.removeChild(element.previousSibling);
        // Supprime les 4 champs
        formulaire.removeChild(element.nextSibling);

        // Supprime le bouton de suppression
        formulaire.removeChild(element);
      }


