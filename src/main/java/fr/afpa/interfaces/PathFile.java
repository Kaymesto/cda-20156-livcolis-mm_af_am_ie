package fr.afpa.interfaces;
/**
 * 
 * @author adrien
 *les diffrentes pages jsp
 */
public interface PathFile {
	//-----------------------------point relais--------------------------------
	public final String POINTRELAIS_POINTRELAISACCUEIL="accueilPointRelais";
	public final String POINTRELAIS_VERSLEFORMCREATION="formulaireCreationPointRelais";
	public final String POINTRELAIS_VERSLEFORMMODIF="pointRelaisModifForm";
	public final String POINTRELAIS_VERSLEFORMSUPP="pointRelaisSuppForm";
	public final String POINTRELAIS_VERSLEFORMRECH="pointRelaisFormRecherche";
	public final String POINTRELAIS_VERSRESULTRECHERCHE="pointRelaisResultatRecherche";
	public final String POINTRELAIS_DECONNEXION="home";
}
