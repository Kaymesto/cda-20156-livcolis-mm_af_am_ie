package fr.afpa.model;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.remoting.RemoteTimeoutException;

public class GestionMail {
	
	/**
	 * @author Ebrahim
	 * Envoi de mail 
	 * @param destinataire
	 * @param etape
	 */
	public static void gestionMail(String destinataire, String etape) {
	    // 1 -> Cr�ation de la session
	    Properties prop = new Properties();
	    prop.put("mail.smtp.auth", true);
	    prop.put("mail.smtp.starttls.enable", "true");
	    prop.put("mail.smtp.host", "smtp-mail.outlook.com");
	    prop.put("mail.smtp.port", "587");

	    
	    String monMail = "livColis@hotmail.com";
	    String motDePasse = "liv_ColisCDA20156";
	    
	    Authenticator auth = new Authenticator() {
	    	protected PasswordAuthentication getPasswordAuthentication() {
	    		return new PasswordAuthentication(monMail,motDePasse);
	    	}
	    };
	    
	    Session sessionMail = Session.getInstance(prop, auth);	

	    try {
			Message leContenu = new MimeMessage(sessionMail);
			
			leContenu.setFrom(new InternetAddress(monMail));
			
			leContenu.setRecipients(Message.RecipientType.TO,
									InternetAddress.parse(destinataire));
		
	    leContenu.addRecipients(Message.RecipientType.CC,
	    						InternetAddress.parse(destinataire));
	    
	    leContenu.setSubject("Suivi de votre Colis");
	    
	    leContenu.setText(etape);
	    
	    Transport.send(leContenu);
	    
	    System.out.println("Message parti par voie num�rique.");
	    
	    } catch (Exception e) {
	    	throw new RuntimeException(e);
	    } 
	}
	
//		public static void main (String [] args) {
//			GestionMail test = new GestionMail();
//			test.gestionMail();
//		}


}
