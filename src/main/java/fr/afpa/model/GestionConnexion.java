package fr.afpa.model;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Transporteur;
import fr.afpa.repository.dao.ConnexionRepository;
import fr.afpa.repository.dao.TransporteurRepository;

@Service
public class GestionConnexion {
	
	@Autowired
	ConnexionRepository connexionRepository;
	@Autowired
	TransporteurRepository transporteurRepository;
	
	/**
	 * @author Mohamed
	 * Verifie que les identifiants de connexion sont bien enregistré en BDD
	 * @param login
	 * @param mdp
	 * @return retourne un transporteur si oui ou un null si non
	 */
	public Transporteur checkAccess (String login, String mdp) {
		
		Optional<Transporteur>trans = Optional.empty();
		try {
			Authentification auth = connexionRepository.findByLoginAndPassword(login, mdp);
			trans =  Optional.ofNullable(transporteurRepository.findByAuth(auth));
			if (!trans.isPresent()) {
				return null;
				
				
			}
		
		}
		catch(Exception e) {
				return null;
			}
		return trans.get();
		}
	}
