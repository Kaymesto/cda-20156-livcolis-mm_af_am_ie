package fr.afpa.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.PointRelais;
import fr.afpa.repository.dao.AdressesRepository;
import fr.afpa.repository.dao.PointRelaisRepository;

/**
 * @author Adrien
 * ici classe metier permettant de recuperer toutes les requetes et les traiter ...
 * @author adrien
 *
 */

@Service
public class GestionPointRelais {
	@Autowired
	private PointRelaisRepository pointRelaisRepository;

	/**
	 * service recuperant la liste de ts les points relais
	 * @return methode pour traiter tous les point relais
	 */
	public ArrayList<PointRelais> requeteAfficherAllPtRelais() {
		// creation d une liste pour recup la liste des pts relais
		// appelle de la requete findByAll
		ArrayList<PointRelais>listePtRelais = (ArrayList<PointRelais>) pointRelaisRepository.findAll();

		return listePtRelais;
	}

	// methode pour enregistrer les points relais
	/**
	 * @author Adrien
	 * @param recuperation en parametre de l objet point relais pour l enregistrer
	 *                     ds la table
	 * @return methode pour enregistrer les points relais
	 */
	public ArrayList<PointRelais> enregistrementPtRelais(PointRelais pointRelais) {
		// Enregistrement du point relais
		pointRelaisRepository.save(pointRelais);
		// renvoyer la liste des points relais
		ArrayList<PointRelais> listePtRelais = (ArrayList<PointRelais>) pointRelaisRepository.findAll();

		return listePtRelais;
	}
/**
 * @author Adrien
 * Service de reuperer un point relais a partir de son id
 * @param id du point relais 
 * @return l objet pt relais recuperer apres une requete au pres de la table 
 */
	public Optional<PointRelais> recupererPtRelais(int id) {
		// recuperation du point relais a partir de l id
		Optional<PointRelais> ptRelais = pointRelaisRepository.findById(id);

		return ptRelais;

	}
/**
 * @author Adrien
 * Service peremttant a la fois la mise a jour des modif du pt relais et afficher une liste de point relais apres modif
 * @param pointRelais a modifier
 * @return la liste des points relais apres modification
 */
	public ArrayList<PointRelais> modifPtRelais(PointRelais pointRelais) {
		// Enregistrement du point relais
		pointRelaisRepository.saveAndFlush(pointRelais);
		// renvoyer la liste des points relais mis a jour
		ArrayList<PointRelais> listePtRelais = (ArrayList<PointRelais>)pointRelaisRepository.findAll();

		return listePtRelais;
	}
/**
 * @author Adrien
 * Service permettant de supp un point relais 
 * @param pointRelais  le point relais a supp
 * @return recuperation de la liste de points relais mis a jour
 */
	public ArrayList<PointRelais> suppPtRelais(PointRelais pointRelais) {
		// supp du point relais
		pointRelaisRepository.deleteById(pointRelais.getIdPointRelais());
		
		// renvoyer la liste des points relais mis a jour
		ArrayList<PointRelais> listePtRelais = (ArrayList<PointRelais>) pointRelaisRepository.findAll();

		return listePtRelais;
	}
/**
 * @author Adrien
 * Service permettant d effectuer la recherche a partir des parametres suivants :
 * @param nom pour recuperer le nom d un point relais
 * @param tel pour recuperer le tel d un point relais
 * @param cp pour recuperer le code postal d un point relais
 * @param ville pour recuperer la ville d un point relais
 * @return une arrayList des points relais en fonction de la rentree des parametres tel,nom,cp et ville
 */
	public ArrayList<PointRelais> recherchePtRelais(String nom, String tel, String cp, String ville) {
                          
		ArrayList<PointRelais> ptRelaisrecherche = (ArrayList<PointRelais>) pointRelaisRepository
				.findByNomPtRelaisStartingWithIgnoreCaseAndTelephoneStartingWithIgnoreCaseAndAdresseCodePostalStartingWithIgnoreCaseAndAdresseVilleStartingWithIgnoreCase(
						nom, tel, cp, ville);
		return ptRelaisrecherche;
	}
/**
 * @author Adrien
 * Service permettant de concatener les horaires des poinnts relais
 * @param ouvertureMatin recuperation de l horaire d ouverture du matin
 * @param fermetureMatin  recuperation de l horaire de fermeture du matin
 * @param ouverturePM recuperation de l horaire d ouverture de l apres midi
 * @param fermeturePM recuperation de l horaire de fermeture de l apres midi
 * @return un string comprenant les horaires des points relais pr pvoir les afficher ds un tableau
 */
	public String concatenationHoraire(String ouvertureMatin, String fermetureMatin, String ouverturePM,
			String fermeturePM) {
		StringBuffer horaire = new StringBuffer();
		horaire.append(ouvertureMatin + "h-" + fermetureMatin + "h/" + ouverturePM + "h-" + fermeturePM + "h");
		// tranformation stringBuffer en string
		String ouverture = horaire.toString();
		return ouverture;
	}
/**
 * @author Adrien
 * Service permettant de trier par nom les points relais 
 * @return une arrayList par odre ascendant alphabetique des point relais trier
 */
	public ArrayList<PointRelais> triePointRelaisNom() {
		ArrayList<PointRelais> listeTrier = (ArrayList<PointRelais>) pointRelaisRepository
				.findAll(Sort.by("nomPtRelais").ascending());
		return listeTrier;
	}
/**
 * @author Adrien
 * service de mise a jour du point relais 
 * @param pr
 */
	public void mAJ(PointRelais pr) {
		
		pointRelaisRepository.saveAndFlush(pr);
	}

public Page<PointRelais> requeteAfficherAllPtRelais(Pageable pageable) {
	
	Page<PointRelais> listeRelais = pointRelaisRepository.findAll(pageable);
	
			return listeRelais;
}

	
}
