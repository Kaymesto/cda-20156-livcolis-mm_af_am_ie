package fr.afpa.model;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.beans.JournalDeBord;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;
import fr.afpa.repository.dao.JournalDeBordRepository;

/**
 * 
 * @author Amal
 *Ici nous utilisons la classe GestionClient qui sera utilisée en couche secondaire afin de respecter le patter mvc.
 *Concrètement cette classe va faire le lien entre la DAO et les controllers.
 */

@Service
public class GestionClient {
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private ColisRepository colisRepository;
	@Autowired
	private JournalDeBordRepository journalDeBordRepository;
	

   
	//---------------------------------- ENREGISTRER NOUVEAU CLIENT  ------------------------------//	
		
		/**
		 * @author Amal
		 * Enregistrement de client
		 * @param Récupération des attributs du nouveau client avec une méthode de set grâce à une instanciation d'un nouveau client et sauvegarde dans la base de données.
		 * @return boolean
		 */
	
	
	public boolean enregistrerNouveauClient(String nom,String prenom,String mail,String telephone) {
		
		Client client = new Client();
		client.setNom(nom);
		client.setPrenom(prenom);
		client.setMail(mail);
		client.setTelephone(telephone);
			
		clientRepository.save(client);
		
		

		return true;
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	//---------------------------------- AFFICHER DES CLIENTS  ------------------------------//	
	
			/**
			 * @author Amal
			 * Récupération de tous les clients
			 * @param Récupération d'une liste de clients dans la base donnée que nous allons stocker grâce à une arraylist instancié auparavant.
			 * @return Une liste de clients 
			 */
	
	
	public ArrayList<Client> afficherDesClients() {
		
		ArrayList<Client> listeClients = new ArrayList<Client>();
		
	listeClients = (ArrayList<Client>) clientRepository.findAll(Sort.by("nom"));
	

		return listeClients;
	}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	//---------------------------------- SUPPRIMER DES CLIENTS  ------------------------------//	
	
	/**
	 * @author Amal
	 * Supression d'un client par identifiant
	 * @param Récupération d'un id pour une suppression dans la base de donnée.
	 * @return Boolean 
	 */
 
	
	public boolean supprimerClient(int idClient) {
		Optional<Client> c = clientRepository.findById(idClient);
		if(c.isPresent()==true) {
			Client client = c.get();
			ArrayList<Colis> listeColis = colisRepository.findByIdDeExpediteur(client);
			client.setListeColisExpedie(listeColis);
			for (Colis colis : client.getListeColisExpedie()) {
				colis.setListeJournalDeBord(journalDeBordRepository.findByColisIdColis(colis.getIdColis()));
				for (JournalDeBord journalDeBord : colis.getListeJournalDeBord()) {
					journalDeBordRepository.delete(journalDeBord);
				}
				colis.setListeJournalDeBord(new ArrayList<JournalDeBord>());
				colisRepository.delete(colis);
			}
			
			ArrayList<Colis> listeColis2 = colisRepository.findByIdDuDestinataire(client);
			client.setListeColisRecu(listeColis2);
			for (Colis colis : client.getListeColisRecu()) {
				colis.setListeJournalDeBord(journalDeBordRepository.findByColisIdColis(colis.getIdColis()));
				for (JournalDeBord journalDeBord : colis.getListeJournalDeBord()) {
					journalDeBordRepository.delete(journalDeBord);
				}
				colis.setListeJournalDeBord(new ArrayList<JournalDeBord>());
				colisRepository.delete(colis);
			}
			client.setListeColisRecu(new ArrayList<Colis>());
			client.setListeColisExpedie(new ArrayList<Colis>());
			clientRepository.saveAndFlush(client);
			clientRepository.deleteById(idClient);
		}
		
		
		
		return true;
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	
	//---------------------------------- MODIFIER DES CLIENTS  ------------------------------//	
	
	/**
	 * @author Amal
	 * Modification de client
	 * @param Récupération des attributs du client mais cette fois avec l'attribut idClient en plus afin de spécifier de quel client il s'agit avec une méthode de set grâce à une instanciation d'un nouveau client et sauvegarde dans la base de données.
	 * @return Void 
	 */
 
	
		public void modifierClient(int idClient,String nom,String prenom,String mail,String telephone) {
			
			
			
			Client client= new Client();
			client.setIdClient(idClient);
			client.setNom(nom);
			client.setPrenom(prenom);
			client.setMail(mail);
			client.setTelephone(telephone);
			
			
			clientRepository.saveAndFlush(client);
		
		}
		
			/**
			 * @author Amal
			 * Récupération d'un client par id avec boolean
			 * @param Récupération de l'attribut idClient afin de cibler de quel client il s'agit
			 * @return Boolean 
			 */
		public boolean modifier(int idClient) {
			 Optional<Client> client =(Optional<Client>) clientRepository.findById(idClient);
			
			return true;
			
		}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		
		//---------------------------------- CHERCHER DES CLIENTS  ------------------------------//	

		
		/**
		 * @author Amal
		 * Récupération de client par id avec colis en retour
		 * @param Récupération de l'attribut idClient afin de cibler de quel client il s'agit
		 * @return Boolean
		 */
	 			
		public Client recuperationClient(int idClient) {
			 Optional<Client> client =(Optional<Client>) clientRepository.findById(idClient);
			  return client.get();

		}
		
		/**
		 * @author Amal
		 * Récupération de client par mail
		 * @param Récupération de l'attribut idClient afin de cibler de quel client il s'agit
		 * @return Boolean
		 */
		public Optional<Client> findClientByEmail (String mail) {
			 Optional<Client> client = clientRepository.findByMail(mail);
			 return client;

		}
		
		

		
		/**
		 * @author Amal
		 * Recherche de client par nom
		 * @param Récupération des attributs nom et prenom client afin d'aller récupérer la liste de clients ayant le même nom et prénom
		 * @return Une arrayList de clients
		 */
		
		public ArrayList<Client> chercherUnClientParNom(String nom,String prenom ) {
			
			ArrayList<Client> client = new ArrayList<Client>();
			
			client =(ArrayList<Client>) clientRepository.findByNomContainingAndPrenomContaining(nom,prenom);

			return client;
		}

		/**
		 * @author Amal
		 * Afficher des clients par page
		 * @param pageable
		 * @return
		 */
		public Page<Client> afficherDesClients(Pageable pageable) {
			Page<Client> listeClient = clientRepository.findAll(pageable);
			return listeClient;
		}
}
