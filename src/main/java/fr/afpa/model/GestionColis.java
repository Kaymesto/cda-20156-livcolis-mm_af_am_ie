package fr.afpa.model;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.beans.JournalDeBord;
import fr.afpa.beans.Transporteur;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;

@Service
public class GestionColis {
	
	@Autowired
	ColisRepository colisRepository;
	
	@Autowired
	ClientRepository clientRepository;
	
	/**
	 * @author Ebrahim
	 * Création de colis
	 * @param poids
	 * @param prix
	 * @param idExp
	 * @param idDest
	 * @param idTrans
	 * @return
	 */
	public Colis creerColis(float poids, float prix, Client idExp, Client idDest, Transporteur idTrans) {
		
		Colis colis = new Colis();
		colis.setPoids(poids);
		colis.setPrix(prix);
		colis.setIdDuDestinataire(idDest);
		colis.setIdDeExpediteur(idExp);
		colis.setIdDuTransporteur(idTrans);
		colis.setListeJournalDeBord(new ArrayList<JournalDeBord>());
		idExp.setListeColisExpedie(new ArrayList<Colis>());
		idExp.ajoutColisExpedie(colis);
		idDest.setListeColisRecu((new ArrayList<Colis>()));
		idDest.ajoutColisRecu(colis);
		idTrans.setListeColis(new ArrayList<Colis>());
		idTrans.ajoutColisTransporte(colis);
		try {
			colisRepository.save(colis);
		
			
			
		}catch(Exception e) {
			return null;
		}
		
		return colis;
	}

	/**
	 * @author Ebrahim
	 * Récupération de colis par Id
	 * @param id
	 * @return
	 */
	public Colis recupColisById(int id) {
		Optional<Colis> colis = colisRepository.findById(id);
		
		return colis.get();
	}
	
	

	

	/**
	 * @author Ebrahim 
	 * Ajout De Colis
	 * @param colis
	 * @return
	 */
	public boolean addColis(Colis colis) {
		try {
			colisRepository.save(colis);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * @author Ebrahim Modification Colis existant
	 * @param colis
	 * @return
	 */
	public boolean modifColis(Colis colis) {

		try {
			colisRepository.saveAndFlush(colis);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * @author Ebrahim 
	 * Supression Colis
	 * @param id
	 * @return
	 */
	public boolean supressionColis(int id) {

		try {
			colisRepository.deleteById(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * @author Ebrahim
	 * Affichage de tous les Colis
	 * @param id
	 * @return
	 */
	public ArrayList <Colis> affichageAllColis() {
		ArrayList <Colis> listeColis = null;
		try {
			listeColis = (ArrayList<Colis>)colisRepository.findAll();
		}
		catch(Exception e) {
			return new ArrayList<Colis>();
		}
			return listeColis;
	}
	
	/**
	 * @author Ebrahim
	 * Affichage Colis du transporteur
	 * @param id
	 * @return
	 */
	public ArrayList <Colis> affichageColisTransporteur(int idTransporteur) {
		ArrayList <Colis> listeColis = null;
		try {
			listeColis = (ArrayList<Colis>)colisRepository.findByIdDuTransporteurIdTransporteur(idTransporteur);
		}
		catch(Exception e) {
			return new ArrayList<Colis>();
		}
			return listeColis;
	}
	
	/**
	 * @author Ebrahim
	 * Affichage Colis de l'exp�diteur
	 * @param id
	 * @return
	 */
	public ArrayList <Colis> affichageColisExpediteur(int idExpediteur) {
		ArrayList <Colis> listeColis = null;
		Client c = clientRepository.findById(idExpediteur).get();
		try {
			listeColis = (ArrayList<Colis>)colisRepository.findByIdDeExpediteur(c);
		}
		
		
		
		catch(Exception e) {
			return new ArrayList<Colis>();
		}
			return listeColis;
	}

	/**
	 * @author Ebrahim
	 * Récupération Optional Colis
	 * @param idColis
	 * @return
	 */
	public Optional<Colis> recupererColisParId(int idColis) {

		Optional<Colis> colisOpt = colisRepository.findById(idColis);
		
		return colisOpt;
	}



/**
 * @author Ebrahim
 * R�cup�ration colis par codeBarre
 * @param codeBarre
 * @return
 */
	public Colis recupColisByCodeBarre(int codeBarre) {

		Colis colis = colisRepository.findByCodeBarre(codeBarre);
		
		return colis;
	}
	
	/**
	 * @author Ebrahim
	 * Recupération liste Colis
	 * @param mailExp
	 * @param mailDest
	 * @return
	 */
	public ArrayList<Colis> recupColisRecherche(String mailExp, String mailDest){
		ArrayList <Colis> listeColis = new ArrayList<Colis>();
		listeColis =(ArrayList<Colis>) colisRepository.findByIdDeExpediteurMailContainingIgnoreCaseAndIdDuDestinataireMailContainingIgnoreCase( mailExp, mailDest);
		
		return listeColis;
	}

}
