package fr.afpa.model;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Transporteur;
import fr.afpa.repository.dao.TransporteurRepository;

@Service
public class GestionTransporteur {
	
	@Autowired
	TransporteurRepository transporteurRepository;
	
	
	/**
	 * @author Mohamed
	 * Méthode permettant de créer un nouveau transporteur
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param tel
	 * @param login
	 * @param mdp
	 * @return retourne true si le transporteur a bien été crée et false dans le cas contraire
	 */
	public boolean createTransporteur (String nom, String prenom, String mail, String tel,String login, String mdp) {
		
		try {
		
		Authentification auth = new Authentification(login,mdp);
		
		Transporteur trans = new Transporteur(nom,prenom,mail,tel);
		trans.setAuth(auth);
		
		transporteurRepository.save(trans);
		
		} catch (Exception e) {
			return false;
		}
		
		return true;
		
	}
	
	/**
	 * @author Ebrahim
	 * test creation transporteur
	 * @param trans
	 * @return
	 */
	public boolean createTransporteur (Transporteur trans) {
		
		
		transporteurRepository.save(trans);
		
		
		
		return true;
		
	}

	/**
	 * @author Mohamed
	 * Methode permettant de retrouver un transporteur à l'aide de son e-mail
	 * @param mail
	 * @return renvoie le transporteur s'il est trouvé ou un null si non
	 */

	public Transporteur findByMail(String mail) {
		
		Optional<Transporteur>trans = Optional.empty();
		try {
			
			trans =  Optional.ofNullable(transporteurRepository.findByMail(mail));
			if (!trans.isPresent()) {
				return null;
				
			}
		}
			catch(Exception e) {
				return null;
			}
		
		return trans.get();
	}
	
	/**
	 * @author Mohamed
	 * Methode permettant de retrouver un transporteur à l'aide de ses identifiants
	 * @param auth
	 * @return renvoie le transporteur s'il est trouvé ou un null si non
	 */

	public Transporteur findByAuth(Authentification auth) {
		
		Optional<Transporteur>trans = Optional.empty();
		
		try {
	
			trans =  Optional.ofNullable(transporteurRepository.findByAuth(auth));
			if (!trans.isPresent()) {
				return null;
				
			}
		}
			catch(Exception e) {
				return null;
			}
		
		return trans.get();
	}

}
