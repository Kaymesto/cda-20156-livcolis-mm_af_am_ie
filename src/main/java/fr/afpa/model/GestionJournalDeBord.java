package fr.afpa.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.beans.Colis;
import fr.afpa.beans.JournalDeBord;
import fr.afpa.beans.PointRelais;
import fr.afpa.repository.dao.JournalDeBordRepository;

@Service
public class GestionJournalDeBord {
	
	@Autowired
	JournalDeBordRepository journalDeBordRepository;
	
	
	/**
	 * @author Ebrahim
	 * Enregistrement Journal de Bord
	 * @param journal
	 * @return
	 */
	public boolean saveJournal(JournalDeBord journal) {
		
		
		try {
		journalDeBordRepository.save(journal);
		}
		
		catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * @author Ebrahim 
	 * Ajout De Journal De Bord
	 * @param colis
	 * @return
	 */
	public boolean addJournalDeBord(JournalDeBord journalDeBord) {
		try {
			journalDeBordRepository.save(journalDeBord);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * @author Ebrahim 
	 * Modification JournalDeBord existant
	 * @param colis
	 * @return
	 */
	public boolean modifJournalDeBord(JournalDeBord journalDeBord) {

		try {
			journalDeBordRepository.saveAndFlush(journalDeBord);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * @author 
	 * Ebrahim Supression Colis
	 * @param id
	 * @return
	 */
	public boolean supressionJournalDeBord(int id) {

		try {
			journalDeBordRepository.deleteById(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * @author Ebrahim
	 * Récupération d'une liste de journal de bord lié à un point relais
	 * @param pointRelais
	 * @return
	 */
	public List<JournalDeBord> recupererJdb(PointRelais pointRelais) {
		
		ArrayList <JournalDeBord> listeJdb = journalDeBordRepository.findByPointRelaisIdPointRelais( pointRelais.getIdPointRelais());
		
		return listeJdb;
	}
	
	/**
	 * @author Ebrahim
	 * Récupération d'une liste de journal de bord lié à un colis
	 * @param pointRelais
	 * @return
	 */
public List<JournalDeBord> recupererJdbAvecColis(Colis colis) {
		
		ArrayList <JournalDeBord> listeJdb = journalDeBordRepository.findByColisIdColis( colis.getIdColis());
		
		return listeJdb;
	}
	
	
}
