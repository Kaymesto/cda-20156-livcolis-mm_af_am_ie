package fr.afpa.beans;
/**
 * equipe am, mm, af, ie
 * entite persistante journal de bord
 * 
 */
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class JournalDeBord implements Comparable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idJournalDeBord_generator")
	@SequenceGenerator(name="idJournalDeBord_generator", sequenceName = "idJournalDeBord_seq", initialValue =1, allocationSize =1)
	private int idJournalDeBord;
	private String dateArrivee;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="idColis")
	private Colis colis;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="idPointRelais")
	private PointRelais pointRelais;
	
	
	@Override
	public int compareTo(Object arg0) {
		
		if(this.getIdJournalDeBord() < ((JournalDeBord) arg0).getIdJournalDeBord())
		
		return -1;
		
		else {
			if(this.getIdJournalDeBord() > ((JournalDeBord) arg0).getIdJournalDeBord())
		
		
		return 1;
		}
		return 0;
	}
}
