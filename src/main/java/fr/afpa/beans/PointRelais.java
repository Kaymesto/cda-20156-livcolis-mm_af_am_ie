package fr.afpa.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author equipe am, mm, af, ie entite persistante pointrelais
 */
@Getter
@Setter
@AllArgsConstructor

@Entity
public class PointRelais {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idPointRelais_generator")
	@SequenceGenerator(name = "idPointRelais_generator", sequenceName = "idPointRelais_seq", initialValue = 1, allocationSize = 1)
	private int idPointRelais;
	@Column(nullable = false)
	private String nomPtRelais;
	@Column(nullable = false)
	private String horaireOuverture;
	@Column(nullable = false)
	private String telephone;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "idAdresse")
	private Adresse adresse;
	@OneToMany(mappedBy="pointRelais", fetch = FetchType.EAGER)
	private List<JournalDeBord> listeJDB;



	public PointRelais() {
		this.listeJDB = new ArrayList<JournalDeBord>();
	}

}