package fr.afpa.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * 
 * @author equipe am, mm, af, ie
 *entite persistante client
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor


@Entity
public class Client {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_generator")
	@SequenceGenerator(name="client_generator", sequenceName = "client_seq", initialValue =1, allocationSize =1)
	private int idClient;
	private String nom;
	private String prenom;
	@Column(unique=true)
	private String mail;
	private String telephone;
	@OneToMany(mappedBy="idDeExpediteur")
	@Column(name="idExpediteur")
	private List<Colis> listeColisExpedie;
	@OneToMany(mappedBy="idDuDestinataire")
	@Column(name="idDestinataire")
	private List<Colis> listeColisRecu;
	
	public Client(String nom,String prenom,String mail,String telephone) {
		
	}
	
	public Client(int idClient,String nom,String prenom,String mail,String telephone) {
		
	}

	public void ajoutColisExpedie(Colis colis) {
		this.listeColisExpedie.add(colis);
	}
	
	public void ajoutColisRecu(Colis colis) {
		this.listeColisRecu.add(colis);
	}

}
