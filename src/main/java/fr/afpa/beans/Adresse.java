package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author adrien entite persistant de l adresse
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = { "numero", "rue", "codePostal", "ville", "pays" })

@Entity
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idAdresse_generator")
	@SequenceGenerator(name = "idAdresse_generator", sequenceName = "idAdresse_seq", initialValue = 1, allocationSize = 1)
	private int idAdresse;
	@Column(nullable = false)
	private int numero;
	@Column(nullable = false)
	private String rue;
	@Column(nullable = false)
	private String codePostal;
	@Column(nullable = false)
	private String ville;
	@Column(nullable = false)
	private String pays;
	@OneToOne(mappedBy = "adresse")
	private PointRelais pointRelais;

}


