package fr.afpa.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * 
 * @author equipe am, mm, af, ie
 *entite persistante transporteur
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
public class Transporteur {

	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idTransporteur_generator")
	@SequenceGenerator(name="idTransporteur_generator", sequenceName = "idTransporteur_seq", initialValue =1, allocationSize =1)
	private int idTransporteur;
	private String nom;
	private String prenom;
	@Column(unique=true)
	private String mail;
	private String telephone;
	@OneToMany(mappedBy="idDuTransporteur")
	private List<Colis> listeColis;
	@OneToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "authTransporteur")
	private Authentification auth;
	
	public Transporteur(String nom, String prenom, String mail, String tel) {
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephone = tel;
		this.listeColis = new ArrayList<Colis>();
	}
	
	public void ajoutColisTransporte (Colis colis) {
		this.listeColis.add(colis);
	}
	
}
