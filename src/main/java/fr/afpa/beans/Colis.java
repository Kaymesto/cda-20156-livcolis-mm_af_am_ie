package fr.afpa.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * 
 * @author equipe am, mm, af, ie
 *entite persistante colis
 */
@Getter
@Setter

@AllArgsConstructor

@Entity
public class Colis {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idColis_generator")
	@SequenceGenerator(name="idColis_generator", sequenceName = "idColis_seq", initialValue =1, allocationSize =1)
	private int idColis;
	private Float poids;
	private Float prix;
	@Column(unique=true)
	private int codeBarre;
	private static int compteur = 298717260;
	private boolean archive;
	private int etapePointRelais;
	@ManyToOne(
			 fetch = FetchType.EAGER
			)
	@JoinColumn(name="idExpediteur") 
	private Client idDeExpediteur;
	@ManyToOne(
			fetch = FetchType.EAGER
			)
	@JoinColumn(name="idDestinataire")
	private Client idDuDestinataire;
	@ManyToOne(
			 fetch = FetchType.EAGER
			)
	@JoinColumn(name="idTransporteur")
	private Transporteur idDuTransporteur;
	@OneToMany(mappedBy="colis", fetch = FetchType.EAGER)
	private List<JournalDeBord> listeJournalDeBord;
	
	public Colis () {
		this.archive = false;
		this.etapePointRelais = 1;
		this.listeJournalDeBord = new ArrayList<JournalDeBord>();
		this.codeBarre = compteur;
		this.compteur++;
	}



}
