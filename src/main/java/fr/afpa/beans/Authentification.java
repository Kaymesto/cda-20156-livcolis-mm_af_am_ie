package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * 
 * @author equipe am, mm, af, ie
 *Entite persistante authentification
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity
public class Authentification {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auth_generator")
	@SequenceGenerator(name="auth_generator", sequenceName = "auth_seq", initialValue =1, allocationSize =1)
	private int id;
	@Column(unique=true)
	private String login;
	private String password;
	@OneToOne(mappedBy = "auth") 
	private Transporteur authTransporteur;


	public Authentification(String login, String mdp) {
	this.login = login;
	this.password = mdp;
	}

}
