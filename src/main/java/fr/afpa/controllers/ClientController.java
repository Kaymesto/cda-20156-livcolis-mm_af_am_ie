package fr.afpa.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Client;
import fr.afpa.model.GestionClient;
/**
 * 
 * @author Amal
 *Ici nous utilisons la classe ClientController afin de gérer nos vues via des méthodes de Gestion créés dans un autre couche pour respecter le pattern mvc.
 */

@Controller
public class ClientController {
	@Autowired
	private GestionClient gestionClient;
	
	
	
       ///////////////////////////////////////////////////////////////////////////////////////////////////
	  //										  PARTIE CRUD                                          //
     //	   																							  //
	//---------------------------------- AFFICHER LA LISTE DES CLIENTS ------------------------------//	
		
		/**
		 * @author Amal
		 * Cette méthode permet au transporteur de voir la liste des utilisateurs après avoir cliqué sur Gestion Utilisateur.
		 * En somme, grâce à cette méthode le transporteur pourra voir les utilisateurs,ajouter des utilisateurs, modifier et supprimer des utilisateurs.
		 * @param model injectant une liste d'utilisateurs.
		 * @return le model qui redirige a l accueil ou les utilisateurs sont listés.
		 */
	
	
																										
	@GetMapping (value = "/listeClients")
	public ModelAndView accueil(ModelAndView mv) {
		ArrayList<Client> listeClients = (ArrayList<Client>) gestionClient.afficherDesClients();
		mv.addObject("ListeCli", listeClients);
		mv.setViewName("listeClients");
		return mv;
	}
	
	
	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	   ///////////////////////////////////////////////////////////////////////////////////////////////////
	  //										  PARTIE CRUD                                          //
     //	   																							  //
	//---------------------------------- AJOUTER DES CLIENTS ----------------------------------------//	
	
	/**
	 * @author Amal
	 * Cette méthode permet au transporteur de créer un nouveau client.
	 * En somme, grâce à cette méthode le transporteur pourra ajouter des utilisateurs en remplissants des champs appropriés.
	 * @param model injectant un formulare avec les attributs nom,prenom,mail et telephone via un bouton de redirection
	 * @return le model qui redirige a l accueil ou les utilisateurs sont listés ou redirige vers le formulaire si un problème est rencontré.
	 */
	
	
	@GetMapping(value = "/ajouterClient")
	public ModelAndView ajouterClientForlulaire(ModelAndView mv) {
		mv.setViewName("formListeClients");
		return mv;
		
	}
	
			/**
			 * @author Amal
			 * Récupération des infos du formulaire pour la création du client
			 * @param mv
			 * @param nom
			 * @param prenom
			 * @param mail
			 * @param telephone
			 * @return
			 */
	@PostMapping(value = "/ajouterClient" )
	public ModelAndView ajouterClient(ModelAndView mv,@RequestParam(value="nom") String nom, @RequestParam(value="prenom") String prenom,@RequestParam(value="mail") String mail, @RequestParam(value="telephone") String telephone) {
		boolean verification= gestionClient.enregistrerNouveauClient(nom, prenom, mail, telephone);
		
		if(verification == true) {
			mv.setViewName("listeClients");	
			ArrayList<Client> listeClients = (ArrayList<Client>) gestionClient.afficherDesClients();
			mv.addObject("ListeCli", listeClients);
		}
		else {
			mv.setViewName("formListeClients");
		}
		
		return mv;
	}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	   ///////////////////////////////////////////////////////////////////////////////////////////////////
	  //										  PARTIE CRUD                                          //
     //	   																							  //
	//---------------------------------- SUPPRIMER DES CLIENTS --------------------------------------//	
	
	/**
	 * @author Amal
	 * Cette méthode permet au transporteur de supprimer des clients.
	 * En somme, grâce à cette méthode le transporteur pourra supprimer un ou des clients via un bouton à cliquer.
	 * @param model dans lequel un chemin est utilisé.L'ID permet de trouver la personne à supprimer et la supprime.
	 * @return le model qui redirige a l accueil ou les utilisateurs sont listés ou à formulaire si un problème est rencontré.
	 */
	@GetMapping(value = "/supprimerClient/{idClient}")
	public ModelAndView supprimerClient(ModelAndView mv,@PathVariable(value = "idClient") String idClient) {
		
		gestionClient.supprimerClient(Integer.parseInt(idClient));
		ArrayList<Client> listeClients = (ArrayList<Client>) gestionClient.afficherDesClients();
		mv.addObject("ListeCli", listeClients);
		mv.setViewName("listeClients");
		return mv;
		
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	   ///////////////////////////////////////////////////////////////////////////////////////////////////
	  //										  PARTIE CRUD                                          //
     //	   																							  //
	//---------------------------------- MODIFIER DES CLIENTS ---------------------------------------//	
	
	/**
	 * @author Amal
	 * Cette méthode permet au transporteur de modifier des informations sur les clients.
	 * En somme, grâce à cette méthode le transporteur pourra mofifier les informations d'un ou des clients via un bouton à cliquer et un formulaire.
	 * @param model dans lequel un chemin est utilisé.L'ID permet de trouver la personne à modifier et ensuite une formulaire de modification lui sera proposé.
	 * @return le model qui redirige a l accueil ou les utilisateurs sont listés.
	 */
	@GetMapping(value = "/modifier/{idClient}")
	public ModelAndView modifier(ModelAndView mv,@PathVariable(value = "idClient") String idClient) {
		boolean afficher = gestionClient.modifier(Integer.parseInt(idClient));
		Client c = gestionClient.recuperationClient(Integer.parseInt(idClient));
		if(afficher == true) {
			mv.setViewName("formModifierClient");
			mv.addObject("client", c);
		}
		else {
			mv.setViewName("listeClients");
		}
		
	
		return mv;
	}
		
		
		/**
		 * @author Amal
		 * Récupérations des infos pour la modification d'un client déja existant
		 * @param mv
		 * @param idClient
		 * @param nom
		 * @param prenom
		 * @param mail
		 * @param telephone
		 * @return
		 */
		@PostMapping(value = "/formModifierClient")
		
		public ModelAndView modifierClient (ModelAndView mv,@RequestParam(value="idClient") int idClient,@RequestParam(value="nom") String nom,@RequestParam(value="prenom") String prenom,@RequestParam(value="mail") String mail,@RequestParam(value="telephone") String telephone){
			gestionClient.modifierClient(idClient, nom, prenom, mail, telephone);
			ArrayList<Client> listeClients = (ArrayList<Client>) gestionClient.afficherDesClients();
			mv.addObject("ListeCli", listeClients);
			mv.setViewName("listeClients");
			
			return mv;
			
		}
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		
		   ///////////////////////////////////////////////////////////////////////////////////////////////////
		  //										  PARTIE CRUD                                          //
	     //	   																							  //
		//---------------------------------- CHERCHER DES CLIENTS ---------------------------------------//	
		
		/**
		 * Cette méthode permet au transporteur de chercher des informations sur les clients.
		 * En somme, grâce à cette méthode le transporteur pourra chercher des informations d'un ou des clients via un bouton à cliquer et un formulaire à la suite du clique.
		 * @param model dans lequel,les attributs nom et prenom sont utilisés afin de trouver un client soit par son nom ou prenom
		 * @return le model qui redirige dans une vue ou les utilisateurs rehcerchés sont listés.
		 */
		@GetMapping(value="/findByNom") 
		public String rechercherClientByNomAndPrenom() {
			return "formChercherClientParNom";
		}
		
		
		
		/**
		 * @author Amal
		 * Récupération formulaire pour la recherche par nom
		 * @param mv
		 * @param nom
		 * @param prenom
		 * @return
		 */
		@PostMapping(value="/findByNom")
		public ModelAndView rechercherClientParNomEtPrenom(ModelAndView mv, @RequestParam(value = "nom") String nom,@RequestParam(value ="prenom") String prenom) {
			
			ArrayList<Client> listeClients = (ArrayList<Client>)gestionClient.chercherUnClientParNom(nom, prenom);
			mv.addObject("ListeClie", listeClients);
			mv.setViewName("listeChercherClientParNom");
			
			
			return mv;
		}
}
