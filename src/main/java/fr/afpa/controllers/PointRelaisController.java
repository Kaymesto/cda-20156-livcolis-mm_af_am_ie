package fr.afpa.controllers;

import java.util.ArrayList;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.JournalDeBord;
import fr.afpa.beans.PointRelais;
import fr.afpa.interfaces.PathFile;
import fr.afpa.model.GestionJournalDeBord;
import fr.afpa.model.GestionPointRelais;

/**
 * 
 * @author adrien Class controler des points relais; class contenant le crud des
 *         points relais reorientees sur les pages jsp
 */
@Controller
public class PointRelaisController {
	@Autowired
	private GestionPointRelais gestionPointRelais;
	@Autowired
	private GestionJournalDeBord gestionJournalDeBord;

//											PARTIE CRUD

//----------------------------------Afficher ts les pts Relais------------------------------//	

	/**
	 * @author Adrien
	 * permet d aller sur la page d accueil des points relais et d afficher ts les
	 * points relais
	 * 
	 * @param model injectant la liste des points relais
	 * @return le model qui redirige a l accueil des points relais
	 */
	@GetMapping(value = "/pointRelaisAccueil")
	public ModelAndView home(ModelAndView model) {
		// creation d une arrayList pour recuperer les info de la requete
		ArrayList<PointRelais> listePointRelais = gestionPointRelais.requeteAfficherAllPtRelais();
		// direction des info vers ma page jsp home des points relais
		model.addObject("listePtRelais", listePointRelais);
		model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);
		
		return model;
	}

	// ----------------------------------Creation d'un point relais------------//

	/**
	 * @author Adrien
	 *permet de rediriger vers la creation d un point relais 
	 * @return permet de rediriger vers le formulaire de creation des points relais
	 */
	@GetMapping(value = "/pointRelaisCreation")
	public String directionFormCreaPtRelais() {

		return PathFile.POINTRELAIS_VERSLEFORMCREATION;
	}

	/**
	 * recuperation des données du form creation point relais
	 * 
	 * @param model          pour recuper les données du formulaire et renvoyer a la
	 *                       vue accueil Point relais
	 * @param ouvertureMatin recuperation des champs du formulaire
	 * @param fermetureMatin recuperation des champs du formulaire
	 * @param ouverturePM    recuperation des champs du formulaire
	 * @param fermeturePM    recuperation des champs du formulaire
	 * @param numeroRue      recuperation des champs du formulaire
	 * @param rue            recuperation des champs du formulaire
	 * @param cp             recuperation des champs du formulaire
	 * @param ville          recuperation des champs du formulaire
	 * @param pays           recuperation des champs du formulaire
	 * @param tel            recuperation des champs du formulaire
	 * @param nom            recuperation des champs du formulaire
	 * @return pour rediriger vers la page d accueil avec l ensemble des points
	 *         relais crées
	 */
	@PostMapping(value = "/enregistrementPointRelais")
	public ModelAndView recupDesDonneesFormCreaPtRelais(ModelAndView model,
			@RequestParam(value = "ouvertureMatin") String ouvertureMatin,
			@RequestParam(value = "fermetureMatin") String fermetureMatin,
			@RequestParam(value = "ouverturePM") String ouverturePM,
			@RequestParam(value = "fermeturePM") String fermeturePM,
			@RequestParam(value = "numeroRue") String numeroRue, @RequestParam(value = "rue") String rue,
			@RequestParam(value = "cp") String cp, @RequestParam(value = "ville") String ville,
			@RequestParam(value = "pays") String pays, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "nom") String nom) {
		// instanciation de l objet Adresse
		Adresse adresse = new Adresse();
		// changement de type pour certaines variables
		int numRue = Integer.parseInt(numeroRue);
		

		adresse.setVille(ville);
		adresse.setRue(rue);
		adresse.setPays(pays);
		adresse.setCodePostal(cp);
		adresse.setNumero(numRue);
		// instanciation de l objet ptRelais
		PointRelais pointRelais = new PointRelais();

		// appel de la methode concatenation des horaires d ouverture
		String horaire = gestionPointRelais.concatenationHoraire(ouvertureMatin, fermetureMatin, ouverturePM,
				fermeturePM);

		// constructeur
		pointRelais.setHoraireOuverture(horaire);
		pointRelais.setAdresse(adresse);
		pointRelais.setTelephone(tel);
		pointRelais.setNomPtRelais(nom);
		// Appel de la methode enregistrement de l objet point relais
		ArrayList<PointRelais> ptRelais = gestionPointRelais.enregistrementPtRelais(pointRelais);
		// redirection vers la page d accueil
		model.addObject("listePtRelais", ptRelais);
		model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);

		return model;
	}

	/**
	 * @author Adrien
	 * @param model objet permettant de rediriger les pages jsp
	 * @return page d accueil qd l utilisateur est sur le formulaire
	 */
	@GetMapping(value = "/retourAccueilPointRelais")
	public ModelAndView retourHome(ModelAndView model) {
		// creation d une arrayList pour recuperer les info de la requete
		ArrayList<PointRelais> listePointRelais = gestionPointRelais.requeteAfficherAllPtRelais();
		// direction des info vers ma page jsp
		model.addObject("listePtRelais", listePointRelais);
		model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);

		return model;
	}

	// ----------------------------------Modification d'un point relais--------------//

	/**
	 * @author Adrien
	 * methode permettant de rediriger sur le form de modif des points relais pre
	 * remplis
	 * 
	 * @param idPointRelais pour recuper le point relais que l utilisateur veut
	 *                      modifier
	 * @param model         pour rediriger vers le formulaire de modif et envoyer
	 *                      les données du pointrelais selectionné ds le form
	 * @return pour rediriger vers la page de form a modifier
	 */
	@GetMapping(value = "/pointRelaisModif/{idPointRelais}")
	public ModelAndView modifPtRelais(@PathVariable String idPointRelais, ModelAndView model) {
		// Changement du type de la variable
		int identifiant = Integer.parseInt(idPointRelais);
		// appel de la methode metier pour recuperer le point relais a partir de l id
		Optional<PointRelais> ptRelais = gestionPointRelais.recupererPtRelais(identifiant);
		if (ptRelais.isPresent()) {
			// redirection des horaires d ouverture vers le formulaire de modification
			String pointRelais = ptRelais.get().getHoraireOuverture();
			String[] horaire = pointRelais.split("h-");
			String horaireDebutMatin = horaire[0];

			String horaireMatinAp = horaire[1];
			String[] midi = horaireMatinAp.split("h/");
			String finMat = midi[0];
			String debutAP = midi[1];

			String horaireFinAP = horaire[2];
			String[] fermeture = horaireFinAP.split("h");
			String finJournee = fermeture[0];

			model.addObject("matin", horaireDebutMatin);
			model.addObject("finAM", finMat);
			model.addObject("debutPM", debutAP);
			model.addObject("fermeture", finJournee);
			model.addObject("pointRelais", ptRelais.get());
			model.setViewName(PathFile.POINTRELAIS_VERSLEFORMMODIF);
		} else {
			ArrayList<PointRelais> listePtRelais = gestionPointRelais.requeteAfficherAllPtRelais();
			model.addObject("listePtRelais", ptRelais);
			model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);

		}

		return model;

	}

	/**
	 * @author Adrien
	 * Methode pour modifier les info du point de relais
	 * 
	 * @param model          model pour recuper les données du formulaire et
	 *                       renvoyer a la vue accueil Point relais
	 * @param ouvertureMatin recuperation des champs du formulaire
	 * @param fermetureMatin recuperation des champs du formulaire
	 * @param ouverturePM    recuperation des champs du formulaire
	 * @param fermeturePM    recuperation des champs du formulaire
	 * @param numeroRue      recuperation des champs du formulaire
	 * @param rue            recuperation des champs du formulaire
	 * @param cp             recuperation des champs du formulaire
	 * @param ville          recuperation des champs du formulaire
	 * @param pays           recuperation des champs du formulaire
	 * @param tel            recuperation des champs du formulaire
	 * @param id             recuperation des champs du formulaire Recuperation des
	 *                       param pour d eventuelles modifs
	 * @return vers la page d accueil
	 */
	@PostMapping(value = "/enregistrementModifPointRelais")
	public ModelAndView recupDesDonneesFormModifPtRelais(ModelAndView model,
			@RequestParam(value = "ouvertureMatin") String ouvertureMatin,
			@RequestParam(value = "fermetureMatin") String fermetureMatin,
			@RequestParam(value = "ouverturePM") String ouverturePM,
			@RequestParam(value = "fermeturePM") String fermeturePM,
			@RequestParam(value = "numeroRue") String numeroRue, @RequestParam(value = "rue") String rue,
			@RequestParam(value = "cp") String cp, @RequestParam(value = "ville") String ville,
			@RequestParam(value = "pays") String pays, @RequestParam(value = "tel") String tel,
			@RequestParam(value = "id") String id, @RequestParam(value = "nom") String nom) {

		// recuperation ds la table de l objet pointRelais a partir de son id
		int idPointRelais = Integer.parseInt(id);
		Optional<PointRelais> pointRelais = gestionPointRelais.recupererPtRelais(idPointRelais);
		if (pointRelais.isPresent()) {

			// changement de type pour certaines variables
			int numRue = Integer.parseInt(numeroRue);

			// modifs des données de l adresse associee au point relais
			pointRelais.get().getAdresse().setCodePostal(cp);
			pointRelais.get().getAdresse().setNumero(numRue);
			pointRelais.get().getAdresse().setVille(ville);
			pointRelais.get().getAdresse().setPays(pays);
			pointRelais.get().getAdresse().setRue(rue);

			// appel de la methode concatenation des horaires d ouverture
			String horaire = gestionPointRelais.concatenationHoraire(ouvertureMatin, fermetureMatin, ouverturePM,
					fermeturePM);

			// constructeur
			pointRelais.get().setHoraireOuverture(horaire);
			pointRelais.get().setTelephone(tel);
			pointRelais.get().setNomPtRelais(nom);

			// Appel de la methode enregistrement et de la mise a jour des modifs de l objet
			// point relais
			ArrayList<PointRelais> ptRelais = gestionPointRelais.modifPtRelais(pointRelais.get());

			// redirection vers la page d accueil
			model.addObject("listePtRelais", ptRelais);
			model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);
		} else {
			ArrayList<PointRelais> listePtRelais = gestionPointRelais.requeteAfficherAllPtRelais();
			model.addObject("listePtRelais", listePtRelais);
			model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);
		}
		return model;
	}

	// ----------------------------------Suppression d'un point relais------------------//

	

	/**
	 * @author Adrien
	 * methode qui recupere l id du point relais que l utilisateur ssouhaite supprimer
	 * 
	 * 
	 * @param model permet de rediriger vers une autre page
	 * @param idPointRelais recuperation de l id du point relais qu on souhaite supprimer
	 * @return vers l accueil point relais
	 */
	@GetMapping(value = "/PointRelaisSupp/{idPointRelais}")
	public ModelAndView suppPtRelais(ModelAndView model, @PathVariable String idPointRelais) {

		// recuperation ds la table de l objet pointRelais a partir de son id
		Optional<PointRelais> pointRelais = gestionPointRelais.recupererPtRelais(Integer.parseInt(idPointRelais));
		
		
		if (pointRelais.isPresent()) {
			pointRelais.get().setListeJDB(gestionJournalDeBord.recupererJdb(pointRelais.get()));
			for(JournalDeBord jdb : pointRelais.get().getListeJDB()) {
				gestionJournalDeBord.supressionJournalDeBord(jdb.getIdJournalDeBord());}
			// Appel de la methode supprimer et de la mise a jour de la liste des objets
			// point relais
			ArrayList<PointRelais> ptRelais = gestionPointRelais.suppPtRelais(pointRelais.get());

			// redirection vers la page d accueil
			model.addObject("listePtRelais", ptRelais);
			model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);
		} else {
			ArrayList<PointRelais> listePtRelais = gestionPointRelais.requeteAfficherAllPtRelais();
			model.addObject("listePtRelais", listePtRelais);
			model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);
		}
		return model;
	}

	// ----------------------------------Recherche d'un point relais------------//

	/**
	 * @author Adrien
	 * Methode permettant de rediriger vers le form de recherche
	 * 
	 * @return vers le form de recherche
	 */
	@GetMapping(value = "/pointRelaisRecherche")
	public String pointRelaisFormRecherche() {
		return PathFile.POINTRELAIS_VERSLEFORMRECH;
	}

	/**
	 * recup des params de l utilisateur pour effectuer la recherche souhaitee
	 * 
	 * @param tel   recherche par numero de tel
	 * @param model pour rediriger vers une page jsp ou injecter des infos
	 * @param ville recherche par ville
	 * @param cp    recherche par code postal
	 * @return vers la page des resultats de recherche
	 */
	@PostMapping(value = "recherchePointRelais")
	public ModelAndView recupElementsRecherchePtRelais(
			@RequestParam(value = "tel") String tel,
			ModelAndView model,
			@RequestParam(value = "ville") String ville,
			@RequestParam(value = "cp") String cp,
			@RequestParam(value = "nom") String nom)
	{

		// appel de la methode recherche
		ArrayList<PointRelais> listerecherches = gestionPointRelais.recherchePtRelais(nom, tel, cp, ville);
		// redirection vers la page de resultat de recherhche
		model.addObject("listePtRelais", listerecherches);
		model.setViewName(PathFile.POINTRELAIS_VERSRESULTRECHERCHE);

		return model;
	}
	// ----------------------------------fin methode crud---------------------//

	// ----------------------Trier par nom les points relais ds un tableau-----------//
/**
 * @author Adrien
 * methode permettant de trier par nom les points relais ds un tableau
 * @param model rediriger vers la page accueil pt relais
 * @return la page point relais accueil
 */
	@GetMapping(value = "/pointRelaisTrierName")
	public ModelAndView trierParNoms(ModelAndView model) {
		ArrayList<PointRelais> listeTriees = gestionPointRelais.triePointRelaisNom();
		model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);
		model.addObject("listePtRelais", listeTriees);
		return model;
	}
/**
 * @author Adrien
 *  methode permettant de trier par nom les points relais ds un tableau
 * @param model rediriger vers la page recherche resultat
 * @return la page resultat recherche
 */
	@GetMapping(value = "/pointRelaisTrierNomRecherche")
	public ModelAndView trierParNomRecherche(ModelAndView model) {
		ArrayList<PointRelais> listeTriees = gestionPointRelais.triePointRelaisNom();
		model.setViewName(PathFile.POINTRELAIS_POINTRELAISACCUEIL);
		model.addObject("listePtRelais", listeTriees);
		return model;
	}
	// ----------------------Deconnexion-----------//
	/**
	 * @author Adrien
	 * Service de deconnexion et renvoie a la page d accueil
	 * @param session pour recuperer la session de l utilisateur 
	 * @param request pour recuperer la session de l utilisateur
	 * @return a la page d accueil
	 */
	@GetMapping(value = "/deconexion")
	public String deconnexion(HttpSession session, HttpServletRequest request) {
		session = request.getSession();
		session.invalidate();
		return PathFile.POINTRELAIS_DECONNEXION;
	}
}
