package fr.afpa.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.beans.JournalDeBord;
import fr.afpa.beans.PointRelais;
import fr.afpa.beans.Transporteur;
import fr.afpa.model.GestionClient;
import fr.afpa.model.GestionColis;
import fr.afpa.model.GestionJournalDeBord;
import fr.afpa.model.GestionMail;
import fr.afpa.model.GestionPointRelais;

@Controller
public class ColisController {
	
	@Autowired
	private GestionColis gestionColis;
	@Autowired
	private GestionClient gestionClient;
	@Autowired
	GestionPointRelais gestionPointRelais;
	@Autowired
	GestionJournalDeBord gestionJournalDeBord;
	GestionMail gestionMail;
	
	
	/**
	 * @author Ebrahim
	 * Affichage des colis liés au transporteur connecté
	 * @return
	 */
	@GetMapping(value="/AccueilModificationColis")
	public String AccueilModificationColis(Model model, HttpSession session) {
		Transporteur trans = (Transporteur) session.getAttribute("trans");
		ArrayList <Colis> listeColis = gestionColis.affichageColisTransporteur(trans.getIdTransporteur());
		for (Colis colis : listeColis) {
			colis.setListeJournalDeBord(gestionJournalDeBord.recupererJdbAvecColis(colis));
		}
		model.addAttribute("listeColis", listeColis);
		if(listeColis.size()>0) {
		model.addAttribute("tailleColis", listeColis.get(0).getListeJournalDeBord().size());}
		return "AccueilModificationColis";
	}
	
	
	/**
	 * @author Ebrahim
	 * Récupération codebarre pour affichage suivi de colis
	 * @param model
	 * @param session
	 * @param codeBarre
	 * @return
	 */
	@GetMapping(value="/affichageSuiviColis/{codebarre}")
	public String affichageSuiviColis(Model model, HttpSession session,@PathVariable(name="codebarre") String codeBarre) {
		int idCodeBarre = 0;
		try {
			idCodeBarre = Integer.parseInt(codeBarre);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Colis c = gestionColis.recupColisByCodeBarre(idCodeBarre);
		model.addAttribute("colis", c);
		return "suiviColis2";	}
	
	
	
	/**
	 * @author Ebrahim
	 * Passage du colis � l'�tape suivante avec envoi de mail � l'exp�diteur et au destinataire
	 * Affichage des colis liés au transporteur connecté
	 * @return
	 */
	@GetMapping(value="/etapeSuivanteColis/{id}")
	public String etapeSuivanteColis(Model model, HttpSession session, @PathVariable(name="id") String id) {
		gestionMail = new GestionMail();
		Transporteur trans = (Transporteur) session.getAttribute("trans");
		int idColis = 0;
		String message = "";
		try {
			idColis = Integer.parseInt(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Colis c = gestionColis.recupColisById(idColis);
		c.setListeJournalDeBord(gestionJournalDeBord.recupererJdbAvecColis(c));
		Collections.sort(c.getListeJournalDeBord());
		if(c.getEtapePointRelais()<c.getListeJournalDeBord().size()-1) {
			int etape = c.getEtapePointRelais()+1;
		c.setEtapePointRelais(etape);
		Date aujourdhui = new Date();
	    DateFormat shortDateFormat = DateFormat.getDateTimeInstance(
	            DateFormat.SHORT,
	            DateFormat.SHORT);
		c.getListeJournalDeBord().get(etape-1).setDateArrivee(shortDateFormat.format(aujourdhui));
		gestionJournalDeBord.modifJournalDeBord(c.getListeJournalDeBord().get(etape-1));
		gestionColis.modifColis(c);
		message = "Votre colis est en cours de transition dans le point relais :"+c.getListeJournalDeBord().get(c.getListeJournalDeBord().size()-1).getPointRelais().getNomPtRelais()+"." 
				+ "\nVous pouvez suivre son avancée avec votre numéro de suivi de colis : "+c.getCodeBarre()+"."+
				"\nMerci et a bientot sur LivColis.";
		gestionMail.gestionMail(c.getIdDeExpediteur().getMail(),message);
		gestionMail.gestionMail(c.getIdDuDestinataire().getMail(),message);}
		
		else if(c.getEtapePointRelais() == c.getListeJournalDeBord().size()-1) {
			int etape = c.getEtapePointRelais()+1;
			c.setEtapePointRelais(etape);
			Date aujourdhui = new Date();
		    DateFormat shortDateFormat = DateFormat.getDateTimeInstance(
		            DateFormat.SHORT,
		            DateFormat.SHORT);
			c.getListeJournalDeBord().get(etape-1).setDateArrivee(shortDateFormat.format(aujourdhui));
			gestionJournalDeBord.modifJournalDeBord(c.getListeJournalDeBord().get(etape-1));
			gestionColis.modifColis(c);
			c.setArchive(true);
			Collections.sort(c.getListeJournalDeBord());
			message ="Votre colis est disponible dans votre point relais : "+c.getListeJournalDeBord().get(c.getListeJournalDeBord().size()-1).getPointRelais().getNomPtRelais();
			gestionMail.gestionMail(c.getIdDeExpediteur().getMail(),message);
			gestionMail.gestionMail(c.getIdDuDestinataire().getMail(),message);}
		

		Collections.sort(c.getListeJournalDeBord());
		
	model.addAttribute("colis", c);
		return "suiviColis2";
	}
	
	/**
	 * @author Ebrahim
	 * Affichage Formulaire de recherche de colis
	 * @param model
	 * @param session
	 * @return
	 */
	@GetMapping(value="/rechercherColis")
	public String rechercherColis(Model model, HttpSession session) {
		Transporteur trans = (Transporteur) session.getAttribute("trans");
		if(trans != null) {
		return "formRechercheColis";}
		else {
			return "home";
		}
	}
	
	/**
	 * @author Ebrahim
	 * Recupération infos formulaire pour la recherche de colis
	 * @param model
	 * @param session
	 * @param idColis
	 * @param mailExp
	 * @param mailDest
	 * @return
	 */
	@PostMapping(value="/rechercherColis")
	public String rechercherColis2(Model model, HttpSession session, @RequestParam(value = "idColis") String idColis,
			@RequestParam(value = "mailExp") String mailExp, @RequestParam(value = "mailDest") String mailDest) {
		Transporteur trans = (Transporteur) session.getAttribute("trans");
		int id = 0;
		try {
			id = Integer.parseInt(idColis);
		} catch (Exception e) {	}
		
		if(trans != null) {
			if(id != 0) {
				ArrayList <Colis> listeColis = new ArrayList();
				listeColis.add(gestionColis.recupColisById(id));
				model.addAttribute("listeColis", listeColis);
				return "AccueilModificationColis";
			}
			else {
			ArrayList <Colis> listeColis = gestionColis.recupColisRecherche(mailExp, mailDest);
			model.addAttribute("listeColis", listeColis);
			return "AccueilModificationColis";}
		}
		else {
			return "home";
		}
	}
	
	

	/**
	 * @author Ebrahim
	 * Suppression de colis par ID
	 * @return
	 */
	@GetMapping(value="/supprimerColis/{id}")
	public String SuppressionColis(Model model, @PathVariable(name="id") String id, HttpSession session) {
		int idColis = 0;
		try {
			idColis = Integer.parseInt(id);
		}
		catch(Exception e) {}
		Colis c = gestionColis.recupColisById(idColis);
		c.setListeJournalDeBord(gestionJournalDeBord.recupererJdbAvecColis(c));
		for (JournalDeBord jdb : c.getListeJournalDeBord()) {
			gestionJournalDeBord.supressionJournalDeBord(jdb.getIdJournalDeBord());
		}
		boolean verif = gestionColis.supressionColis(idColis);
		
		Transporteur trans = (Transporteur) session.getAttribute("trans");
		ArrayList <Colis> listeColis = gestionColis.affichageColisTransporteur(trans.getIdTransporteur());
		model.addAttribute("listeColis", listeColis);
		return "AccueilModificationColis";
	}

	
	/**
	 * @author Ebrahim
	 * Renvoi vers l'accueil de la liste des colis correspondant au transporteur
	 * @return
	 */
	@GetMapping(value="/modifierColis/{id}")
	public String AccueilModificationColis(Model model, @PathVariable(name="id") String id, HttpSession session) {
		int idColis = 0;
		Transporteur t = (Transporteur)session.getAttribute("trans");
		try {
			idColis = Integer.parseInt(id);
		}
		catch(Exception e) {}
		Optional<Colis> colisOpt = gestionColis.recupererColisParId(idColis);
		
		if (colisOpt.isPresent() == true) {
		model.addAttribute("colis", colisOpt.get());
		return "formModifierColis";}
		else {
			ArrayList <Colis> listeColis = gestionColis.affichageColisTransporteur(t.getIdTransporteur());
			model.addAttribute("listeColis", listeColis);
			return "AccueilModificationColis";
		}
	}
	
	/**
	 * @author Ebrahim
	 * Formulaire De Modification de Colis
	 * @param mv
	 * @param idColis
	 * @param poidsColis
	 * @param prixColis
	 * @param codeBarreColis
	 * @param etatColis
	 * @param mailExpedColis
	 * @param mailDestColis
	 * @param etapeRelaisColis
	 * @param session
	 * @return
	 */
	@PostMapping(value="/formModifierColis")
	public ModelAndView formModifierColis(ModelAndView mv, @RequestParam(value = "idColis") String idColis,		
			@RequestParam(value = "poidsColis") String poidsColis, @RequestParam(value = "prixColis") String prixColis,
			@RequestParam(value = "codeBarreColis") String codeBarreColis, @RequestParam(value = "etatColis") String etatColis,
			@RequestParam(value = "mailExpedColis") String mailExpedColis, @RequestParam(value = "mailDestColis") String mailDestColis,
			@RequestParam(value = "etapeRelaisColis") String etapeRelaisColis, HttpSession session) {
		int idDuColis = 0;
		int etapeColis = 0;
		Float poids = 0F;
		Float prix = 0F;
		boolean etat = false;
		int codeBarre = 0;
		int compteur =0;
		try {
			idDuColis = Integer.parseInt(idColis);
			compteur++;
			etapeColis = Integer.parseInt(etapeRelaisColis.split("-")[0]);
			compteur++;
			poids = Float.parseFloat(poidsColis);
			compteur++;
			prix = Float.parseFloat(prixColis);
			compteur++;
			etat = Boolean.parseBoolean(etatColis);
			compteur++;
			codeBarre = Integer.parseInt(codeBarreColis);
		}
		catch (Exception e) {
	
			
		}

		Optional<Client> expediteur = gestionClient.findClientByEmail(mailExpedColis);
		Optional<Client> destinataire = gestionClient.findClientByEmail(mailDestColis);
		if(expediteur.isPresent() && destinataire.isPresent()) {
		Colis colis = new Colis();
		Transporteur t = (Transporteur)session.getAttribute("trans");
		colis.setIdColis(idDuColis);
		colis.setPoids(poids);
		colis.setPrix(prix);
		colis.setArchive(etat);
		colis.setCodeBarre(codeBarre);
		colis.setEtapePointRelais(etapeColis);
		colis.setIdDeExpediteur(expediteur.get());
		colis.setIdDuDestinataire(destinataire.get());
		colis.setIdDuTransporteur(t);
		ArrayList <JournalDeBord> listeJournalDeBord = (ArrayList<JournalDeBord>) gestionJournalDeBord.recupererJdbAvecColis(colis);
		colis.setListeJournalDeBord(listeJournalDeBord);
		gestionColis.modifColis(colis);
		mv.setViewName("ConfirmationModificationColis");
		mv.addObject("colis", colis);
		}
		else {
			Optional<Colis> colisOpt = gestionColis.recupererColisParId(idDuColis);
			
			if (colisOpt.isPresent() == true) {
			mv.addObject("colis", colisOpt.get());
			mv.setViewName("formModifierColis");}
		}
		return mv;
	}
	
	
	/**
	 * @author Mohamed
	 * Methode permettant de sauvegarder un colis à l'aide des clients/ du transporteur et des infos colis
	 * @param mv
	 * @param mailExp
	 * @param mailDest
	 * @param poids
	 * @param prix
	 * @param session
	 * @return
	 */
	@PostMapping(value="/saveColis1")
	public ModelAndView formSaveColisP1(ModelAndView mv,@RequestParam(name="page", defaultValue = "0") int p,
			@RequestParam(value = "mailExp") String mailExp,		
			@RequestParam(value = "mailDest") String mailDest,@RequestParam(value = "poids") String poids,
			@RequestParam(value = "prix") String prix, HttpSession session) {
		
		Optional<Client> expediteur = gestionClient.findClientByEmail(mailExp);
		
		Optional <Client> destinataire = gestionClient.findClientByEmail(mailDest);
		
		
		if(!destinataire.isPresent()) {
			ArrayList <Client> listeClient = gestionClient.afficherDesClients();
			mv.addObject("listeClient",listeClient);
			mv.addObject("destExist", false);
			mv.setViewName("saveColis1");
		}
		
		if(!expediteur.isPresent()) {
			ArrayList <Client> listeClient = gestionClient.afficherDesClients();
			mv.addObject("listeClient",listeClient);
			mv.addObject("expExist", false);
			mv.setViewName("saveColis1");
		}
		
		if ("".equals(prix)) {
			prix = "0";
		}
		
		if(destinataire.isPresent() && expediteur.isPresent()) {
			Transporteur trans = (Transporteur) session.getAttribute("trans");
			
			
			Colis colis = gestionColis.creerColis(Float.parseFloat(poids), Float.parseFloat(prix), expediteur.get(), destinataire.get(), trans );
			if ( colis != null) {
			Page <PointRelais> listeRelais = gestionPointRelais.requeteAfficherAllPtRelais(PageRequest.of(p, 5));
				
			int pageCounts = listeRelais.getTotalPages();
			int [] pages = new int [pageCounts];
				
			for(int i = 0; i < pages.length; i++) pages[i]= i;
			
			
			mv.addObject("listeRelais", listeRelais);
			mv.addObject("pages",pages);
			mv.addObject("colis", colis);
			mv.setViewName("saveColis2");
					}
			
			else if (gestionColis.creerColis(Float.parseFloat(poids), Float.parseFloat(prix), expediteur.get(), destinataire.get(), trans )== null){
				System.out.println("Une erreur a ete detecte");
			}
		}	
		
		return mv;
	}
	
	@GetMapping(value="/saveColis1")
	public ModelAndView SaveColisP1(ModelAndView mv,@RequestParam(name="page", defaultValue = "0") int p,
			@RequestParam(name="idColis") int idColis) {
		
			Page <PointRelais> listeRelais = gestionPointRelais.requeteAfficherAllPtRelais(PageRequest.of(p, 5));
				
			int pageCounts = listeRelais.getTotalPages();
			int [] pages = new int [pageCounts];
				
			for(int i = 0; i < pages.length; i++) pages[i]= i;
			
			Colis colis = gestionColis.recupColisById(idColis);
			
			mv.addObject("listeRelais", listeRelais);
			mv.addObject("pages",pages);
			mv.addObject("colis", colis);
			mv.setViewName("saveColis2");
				
		
		return mv;
	}
	
	
	/**
	 * @author Mohamed
	 * Lien de redirection vers le formulaire d'enregistrement colis
	 * @return
	 */
	@GetMapping(value="/saveColis")
	public ModelAndView saveColis(ModelAndView mv,@RequestParam(name="page", defaultValue = "0") int p) {
		Page<Client> listeClient = gestionClient.afficherDesClients(PageRequest.of(p, 5));
		
		int pageCounts = listeClient.getTotalPages();
		int [] pages = new int [pageCounts];
		
		for(int i = 0; i < pages.length; i++) {pages[i]= i;
		System.out.println(pages[i]);
		
		}
		
		mv.addObject("listeClient",listeClient);
		mv.addObject("pages",pages);
		mv.setViewName("saveColis1");
		
		return mv;
	}
	
	

	/**
	 * @author Mohamed & Ebrahim
	 * Méthode permettant l'itineraire des points relais au colis enregistré a la page précédante
	 * @param mv
	 * @param idRelaisDepart
	 * @param idRelaisArrivee
	 * @param listeRelais
	 * @param idColis
	 * @return
	 * @throws ParseException 
	 */
	@PostMapping(value="/saveColis2")
	public ModelAndView formSaveColisP2(ModelAndView mv, @RequestParam(value = "idDepart") String idRelaisDepart,		
			@RequestParam(value = "idArrivee") String idRelaisArrivee,@RequestParam(value = "champs1[]",required = false) String[] listeRelais,
			@RequestParam(value="idColis") String idColis) throws ParseException{
		
		Colis colis = gestionColis.recupColisById(Integer.parseInt(idColis));
		
		Optional<PointRelais> relaisDepart = gestionPointRelais.recupererPtRelais(Integer.parseInt(idRelaisDepart));
		
		JournalDeBord journalDepart = new JournalDeBord();
		journalDepart.setColis(colis);
		journalDepart.setPointRelais(relaisDepart.get());

	    Date aujourdhui = new Date();
	    DateFormat shortDateFormat = DateFormat.getDateTimeInstance(
	            DateFormat.SHORT,
	            DateFormat.SHORT);
		journalDepart.setDateArrivee(shortDateFormat.format(aujourdhui));
		
		
		
		relaisDepart.get().getListeJDB().add(journalDepart);
		
		colis.getListeJournalDeBord().add(journalDepart);	
		
		gestionJournalDeBord.saveJournal(journalDepart);
		
		
		if(listeRelais != null) {
			for(int i=0; i<listeRelais.length;i++) {
				Optional<PointRelais> relaisInt = gestionPointRelais.recupererPtRelais(Integer.parseInt(listeRelais[i]));
				JournalDeBord journalInt = new JournalDeBord();
				journalInt.setColis(colis);
				journalInt.setPointRelais(relaisInt.get());			
				relaisInt.get().getListeJDB().add(journalInt);
				colis.getListeJournalDeBord().add(journalInt);
				gestionJournalDeBord.saveJournal(journalInt);
			}	
		}
		
		Optional<PointRelais> relaisArrivee = gestionPointRelais.recupererPtRelais(Integer.parseInt(idRelaisArrivee));
		JournalDeBord journalArrivee = new JournalDeBord();
		journalArrivee.setColis(colis);
		journalArrivee.setPointRelais(relaisArrivee.get());
		
		
		
		relaisArrivee.get().getListeJDB().add(journalArrivee);
		colis.getListeJournalDeBord().add(journalArrivee);
		
		gestionJournalDeBord.saveJournal(journalArrivee);
		gestionMail = new GestionMail();
		String message = "Votre colis vient d'etre pris en charge par le point relais :"+colis.getListeJournalDeBord().get(0).getPointRelais().getNomPtRelais()+"." 
				+ "\n\nVous pouvez suivre son avancee avec votre num�ro de suivi de colis : "+colis.getCodeBarre()+"."+
				"\n\nMerci et a bientot sur LivColis.";
		gestionMail.gestionMail(colis.getIdDeExpediteur().getMail(),message);
		gestionMail.gestionMail(colis.getIdDuDestinataire().getMail(),message);	
		
		mv.setViewName("colisConfirme");
		
		return mv;
	}
	
	/**
	 * @author Mohamed
	 * Méthode permettant de suivre un colis a l'aide de son code et de renvoyer vers la page de suivi
	 * @param mv
	 * @param code
	 * @return
	 */
	@PostMapping(value="/suiviColis")
	public ModelAndView suiviColis(ModelAndView mv, @RequestParam(value="suivi") String code) {
		
		Colis colis = gestionColis.recupColisByCodeBarre(Integer.parseInt(code));
		
		Collections.sort(colis.getListeJournalDeBord());
		
		mv.addObject("colis", colis);
		mv.setViewName("suiviColis");
		
		return mv;
	}
}