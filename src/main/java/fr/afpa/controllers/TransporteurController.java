package fr.afpa.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Transporteur;
import fr.afpa.model.GestionAdresse;
import fr.afpa.model.GestionConnexion;
import fr.afpa.model.GestionTransporteur;

@Controller
public class TransporteurController {
	@Autowired
	GestionTransporteur gestionTransporteur;
	
	@Autowired
	GestionConnexion gestionConnexion;
	
	
	/**
	 * @author Mohamed
	 * renvoie au formulaire d'inscription
	 * @return
	 */
	@GetMapping(value="Inscription")
	public String inscription() {
		
		return "formInscription";
	}
	
	
	/**
	 * @author Mohamed
	 * Methode pour enregistrer un transporteur avec différents controles (login/mail) 
	 * @param mv
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param mail2
	 * @param login
	 * @param tel
	 * @param mdp
	 * @param mdp2
	 * @return
	 */
	@PostMapping(value="saveTransporteur")
	public ModelAndView saveTransporteur (ModelAndView mv, @RequestParam(name="nom") String nom, @RequestParam(name="prenom") String prenom,
	@RequestParam(name="mail") String mail,@RequestParam(name="mail2") String mail2,@RequestParam(name="login") String login,
	@RequestParam(name="tel") String tel,@RequestParam(name="mdp") String mdp, @RequestParam(name="mdp2") String mdp2) {
		
		Transporteur transporteur = new Transporteur(nom,prenom,mail,tel);
		Transporteur trans = gestionConnexion.checkAccess(login,mdp);
			
		
		if (trans!=null) {
			mv.addObject("loginExist", true);
			mv.setViewName("formInscription");
			mv.addObject("transporteur", transporteur);
		
		}
		
		else if (gestionTransporteur.findByMail(mail) != null) {
			mv.addObject("mailExist", true);
			mv.setViewName("formInscription");
			
			mv.addObject("transporteur", transporteur);
		}
		
		else if (!(mail.equals(mail2) && mdp.equals(mdp2))) {
			mv.addObject("error", true);
			mv.setViewName("formInscription");
			
			mv.addObject("transporteur", transporteur);
		}
		
		else {
			gestionTransporteur.createTransporteur(nom, prenom, mail, tel, login, mdp);
			mv.setViewName("inscriptionConfirme");
		}
		
		
		return mv;
	

		}
}
