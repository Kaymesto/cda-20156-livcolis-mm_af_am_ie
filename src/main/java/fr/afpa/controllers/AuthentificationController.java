package fr.afpa.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.beans.Transporteur;
import fr.afpa.model.GestionConnexion;

@Controller

public class AuthentificationController {
	
	@Autowired
	GestionConnexion gestionConnexion;
	
	
	/**
	 * @author Mohamed
	 * Renvoi sur la page d'accueil
	 * @return
	 */
	@GetMapping(value="/")
	public String homePage() {
		
		return "home";
	}
	
	/**
	 * @author Mohamed
	 * Lien de redirection vers la page de connexion
	 * @return
	 */
	@GetMapping(value="/AuthentificationPage")
	public String connexionPage() {
		
		return "pageConnexion";
	}
	
	
	/**
	 * @author Mohamed
	 * Lien de redirection vers l'espace-membre
	 * @return
	 */
	@GetMapping(value="/espaceMembre")
	public String espaceMembre() {
		
		return "espace-membre";
		
		
	}
	
	/**
	 * @author Mohamed
	 * Methode verifiant si le login et le mot de passe correspond a un transporteur enregistré et si oui, crée une session avec ce transporteur
	 * @param mv
	 * @param login
	 * @param mdp
	 * @param session
	 * @param request
	 * @return retourne un transporteur ou un null
	 */
	@PostMapping(value="/checkAuth")
	public ModelAndView verifIdentifiants (ModelAndView mv, @RequestParam(name = "login") String login,
			@RequestParam(name = "mdp") String mdp, HttpSession session, HttpServletRequest request) {
		Transporteur trans = null;
		
		if (!"".equals(login) || !"".equals(mdp)) {
			 trans = gestionConnexion.checkAccess(login, mdp);
		}
	
		if (trans != null) {
		
			session = request.getSession();
			session.setAttribute("trans", trans);
			mv.addObject("trans", trans);
			mv.setViewName("espace-membre");
	
		}
	
	
	else {
		
		mv.addObject("errorConnection", true);
		mv.setViewName("pageConnexion");
	}
	
	return mv;

	}
	
}
