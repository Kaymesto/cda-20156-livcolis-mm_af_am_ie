package fr.afpa.repository.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Authentification;
import fr.afpa.beans.PointRelais;
import fr.afpa.beans.Transporteur;
/**
 * 
 * @author adrien
 * requete permettant de trouver a partir du nom, ville,telephone et cp les points relais
 *
 */
public interface PointRelaisRepository extends JpaRepository<PointRelais, Integer> {
	
	/**
	 * @author Adrien
	 * Recherche de Points relais par nom et téléphone et adresse
	 * @param nom
	 * @param telephone
	 * @param codePostal
	 * @param ville
	 * @return
	 */
	List<PointRelais> findByNomPtRelaisStartingWithIgnoreCaseAndTelephoneStartingWithIgnoreCaseAndAdresseCodePostalStartingWithIgnoreCaseAndAdresseVilleStartingWithIgnoreCase(String nom,String telephone,String codePostal,String ville);
	
}
