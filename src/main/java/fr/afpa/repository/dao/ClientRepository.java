package fr.afpa.repository.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Client;

public interface ClientRepository extends JpaRepository<Client, Integer> {
	/**
	 * @author Amal
	 * ICI ON RECHERCHE PAR NOM ET PRENOM EN MÊME TEMPS.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public List<Client> findByNomContainingAndPrenomContaining(String nom,String prenom);
	
	
	/**
	 * @author Amal
	 * ICI ON RECHERCHE UN CLIENT PAR MAIL.
	 * @param mail
	 * @return
	 */
	public Optional<Client> findByMail(String mail);

}
