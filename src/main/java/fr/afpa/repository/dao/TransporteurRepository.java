package fr.afpa.repository.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Transporteur;

public interface TransporteurRepository extends JpaRepository<Transporteur, Integer> {
	
	/**
	 * @author Mohamed
	 * Recherche transporteur par Authentification
	 * @param auth
	 * @return
	 */
	public Transporteur findByAuth(Authentification auth);

	/**
	 * @author Mohamed
	 * Recherche de transporteur par Mail
	 * @param mail
	 * @return
	 */
	public Transporteur findByMail (String mail);
	



}
