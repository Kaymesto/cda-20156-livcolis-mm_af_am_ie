package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Authentification;

public interface ConnexionRepository extends JpaRepository<Authentification, Integer> {
	
	/**
	 * @author Mohamed
	 * Vérification par login et mdp
	 * @param login
	 * @param password
	 * @return
	 */
	public Authentification findByLoginAndPassword(String login, String password);

}
