package fr.afpa.repository.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.JournalDeBord;

public interface JournalDeBordRepository extends JpaRepository<JournalDeBord, Integer> {

	/**
	 * @author Adrien
	 * Recherche de journaux de bord par id de point relais
	 * @param idPointRelais
	 * @return
	 */
	ArrayList<JournalDeBord> findByPointRelaisIdPointRelais(int idPointRelais);

	/**
	 * @author Adrien
	 * Recherche de Journaux de bords par id de colis
	 * @param idColis
	 * @return
	 */
	ArrayList<JournalDeBord> findByColisIdColis(int idColis);

}
