package fr.afpa.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.PointRelais;

public interface AdressesRepository extends JpaRepository<Adresse, Integer> {
	

	
}
