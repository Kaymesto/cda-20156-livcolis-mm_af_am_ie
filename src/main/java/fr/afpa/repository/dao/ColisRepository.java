package fr.afpa.repository.dao;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;

public interface ColisRepository extends JpaRepository<Colis, Integer> {

	/**
	 * @author Ebrahim
	 * Recherche dans la DAO des colis par ID de l'expéditeur
	 * @param idExpediteur
	 * @return
	 */
    ArrayList<Colis> findByIdDeExpediteur(Client idExpediteur);
    
    /**
     * @author Ebrahim
     * Recherche d'un colis par son id
     * @param id
     * @return
     */
    Colis findByIdColis (int id);
    
    /**
     * @author Ebrahim
     * Recherche de colis par id Du transporteur
     * @param idDuTransporteur
     * @return
     */
    ArrayList <Colis> findByIdDuTransporteurIdTransporteur(int idDuTransporteur);
    
    /**
     * @author Ebrahim
     * Recherche de colis par l'email de l'expéditeur et le mail du destinataire
     * @param mail
     * @param mail2
     * @return
     */
    ArrayList <Colis> findByIdDeExpediteurMailContainingIgnoreCaseAndIdDuDestinataireMailContainingIgnoreCase( String mail, String mail2);
    
	Colis findByCodeBarre(int codeBarre);

	ArrayList<Colis> findByIdDuDestinataire(Client client);

//    ArrayList<Colis> findBy******Or******(String ****, Float ******);
//
//    ArrayList<Colis> findBy********LikeIgnoreCase(String *******);
//    
//    ArrayList<Colis> findBy********Between(int *******,int *******);
    
}
