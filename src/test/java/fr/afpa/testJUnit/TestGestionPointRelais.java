package fr.afpa.testJUnit;



import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedList;
import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.PointRelais;
import fr.afpa.repository.dao.PointRelaisRepository;
/**
 * 
 * @author adrien
 * test unitaire sur le package service pointRelais
 *
 */

@RunWith(MockitoJUnitRunner.class)

public class TestGestionPointRelais {
	@InjectMocks
	PointRelais pointRelais;
	@InjectMocks
	Adresse adresse;
	
	@Mock
	PointRelaisRepository pointRelaisRepository;

	@BeforeEach
	public void setUp() throws Exception {
		//creation de l objet point relais et adresse pour les tests
		pointRelais = new PointRelais();
		adresse = new Adresse();
		pointRelais.setIdPointRelais(1);
		pointRelais.setHoraireOuverture("9-12/13-20");
		pointRelais.setNomPtRelais("test");
		pointRelais.setTelephone("0320396302");
		adresse.setCodePostal("test");
		adresse.setNumero(1);
		adresse.setPays("test");
		adresse.setRue("test");
		adresse.setVille("test");
		pointRelais.setAdresse(adresse);
		
	}
/**
 * test sur la methode enregistrement ptrelais
 */
	@Test
	public void testRequeteEnregistrementPtRelais() {
		PointRelaisRepository mockPtRelais = RepositoryFactoryBuilder.builder().mock(PointRelaisRepository.class);
		mockPtRelais.save(pointRelais);
		assertTrue(!mockPtRelais.findAll().isEmpty());
		
		
	}

	@Test
	public void testRequeteAfficherTsLesPtRelais() {
	
		PointRelaisRepository mockPtRelais = RepositoryFactoryBuilder.builder().mock(PointRelaisRepository.class);
		mockPtRelais.save(pointRelais);
		LinkedList<PointRelais>listePtRelais = (LinkedList<PointRelais>) mockPtRelais.findAll();

		assertTrue(listePtRelais.contains(pointRelais));
		
	}
	
	@Test
	public void testRecupererPtRelais() {
		PointRelaisRepository mockPtRelais = RepositoryFactoryBuilder.builder().mock(PointRelaisRepository.class);
		mockPtRelais.save(pointRelais);
		Optional<PointRelais> pointRelais2=mockPtRelais.findById(pointRelais.getIdPointRelais());
		assertEquals(pointRelais2.get(), pointRelais);
	}
	@Test
	public void testModifierPtRelais() {
		PointRelaisRepository mockPtRelais = RepositoryFactoryBuilder.builder().mock(PointRelaisRepository.class);
		pointRelais.setNomPtRelais("nomPtRelais");
		mockPtRelais.saveAndFlush(pointRelais);
		LinkedList<PointRelais>listePtRelais = (LinkedList<PointRelais>) mockPtRelais.findAll();
		assertEquals("nomPtRelais", pointRelais.getNomPtRelais());;
	}
	@Test
	public void testSuppPtRelais() {
		PointRelaisRepository mockPtRelais = RepositoryFactoryBuilder.builder().mock(PointRelaisRepository.class);
		mockPtRelais.save(pointRelais);
		mockPtRelais.delete(pointRelais);
		LinkedList<PointRelais>listePtRelais = (LinkedList<PointRelais>) mockPtRelais.findAll();
		assertTrue(!listePtRelais.contains(pointRelais));
	
	}
	@Test
	public void testRechercherPtRelais() {
		pointRelais = new PointRelais();
		adresse = new Adresse();
		pointRelais.setIdPointRelais(1);
		pointRelais.setHoraireOuverture("9-12/13-20");
		pointRelais.setNomPtRelais("test");
		pointRelais.setTelephone("0320396302");
		adresse.setCodePostal("test");
		adresse.setNumero(1);
		adresse.setPays("test");
		adresse.setRue("test");
		adresse.setVille("test");
		pointRelais.setAdresse(adresse);
		PointRelaisRepository mockPtRelais = RepositoryFactoryBuilder.builder().mock(PointRelaisRepository.class);
		mockPtRelais.save(pointRelais);
		LinkedList<PointRelais>listePtRelais=(LinkedList<PointRelais>) mockPtRelais.findByNomPtRelaisStartingWithIgnoreCaseAndTelephoneStartingWithIgnoreCaseAndAdresseCodePostalStartingWithIgnoreCaseAndAdresseVilleStartingWithIgnoreCase(pointRelais.getNomPtRelais(), pointRelais.getTelephone(),pointRelais.getAdresse().getCodePostal(), pointRelais.getAdresse().getVille());
		assertTrue(listePtRelais.contains(pointRelais));
	
	}
	@Test
	public void testTrierPtRelais() {
		pointRelais = new PointRelais();
		adresse = new Adresse();
		pointRelais.setIdPointRelais(1);
		pointRelais.setHoraireOuverture("9-12/13-20");
		pointRelais.setNomPtRelais("test");
		pointRelais.setTelephone("0320396302");
		adresse.setCodePostal("test");
		adresse.setNumero(1);
		adresse.setPays("test");
		adresse.setRue("test");
		adresse.setVille("test");
		pointRelais.setAdresse(adresse);
		
		
		PointRelais pointRelais2 = new PointRelais();
		Adresse adresse2 = new Adresse();
		pointRelais2.setIdPointRelais(2);
		pointRelais2.setHoraireOuverture("10-12/13-20");
		pointRelais2.setNomPtRelais("atests");
		pointRelais2.setTelephone("0320396302");
		adresse2.setCodePostal("test");
		adresse2.setNumero(1);
		adresse2.setPays("test");
		adresse2.setRue("test");
		adresse2.setVille("test");
		pointRelais2.setAdresse(adresse);
		PointRelaisRepository mockPtRelais = RepositoryFactoryBuilder.builder().mock(PointRelaisRepository.class);
		mockPtRelais.save(pointRelais);
		mockPtRelais.save(pointRelais2);
		LinkedList<PointRelais>listePtRelais=(LinkedList<PointRelais>) mockPtRelais.findAll(Sort.by("nomPtRelais").ascending());
		
		assertEquals(listePtRelais.get(0),pointRelais2);
	
	}
	
	
	@AfterEach
	public void setUpAfterClass() throws Exception {

	}

}
