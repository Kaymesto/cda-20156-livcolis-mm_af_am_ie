package fr.afpa.testJUnit;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.beans.Transporteur;
import fr.afpa.model.GestionClient;
import fr.afpa.model.GestionColis;
import fr.afpa.model.GestionTransporteur;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;
import fr.afpa.repository.dao.JournalDeBordRepository;
import fr.afpa.repository.dao.TransporteurRepository;



@RunWith(MockitoJUnitRunner.class)
public class TestGestionClient {
	@InjectMocks GestionClient gestionClient;
	@Mock ClientRepository clientRepo;	
	
	
	@BeforeEach
	public void setUp() throws Exception {		
	}
	
	@Test
	public void testCreationClient() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		
		mockClientRepository.save(c);
		List <Client> listeClient = mockClientRepository.findAll();
		assertTrue(listeClient.size()>0);}
	

	@Test
	public void testRecupClient() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		
		mockClientRepository.save(c);
		Client client = mockClientRepository.findById(c.getIdClient()).get();
		assertTrue(c.getIdClient()==client.getIdClient());}
	
	@Test
	public void testRecuperationAllClient() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		Client c2 = new Client();
		c.setNom("Meddah");
		c.setPrenom("Mohamed");
		c.setMail("mmeddah59@gmail.com");
		c.setTelephone("0601010101");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		
		mockClientRepository.save(c);
		mockClientRepository.save(c2);
		List <Client> listeClient = mockClientRepository.findAll();
		assertTrue(listeClient.size()==2);}

	
	@Test
	public void testSuppressionClient() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		Client c2 = new Client();
		c.setNom("Meddah");
		c.setPrenom("Mohamed");
		c.setMail("mmeddah59@gmail.com");
		c.setTelephone("0601010101");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		
		mockClientRepository.save(c);
		mockClientRepository.save(c2);
		mockClientRepository.delete(c2);
		List <Client> listeClient = mockClientRepository.findAll();
		assertTrue(listeClient.size()==1);}
	
	
	
	@Test
	public void testModifClient() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		
		mockClientRepository.save(c);
		Client client = mockClientRepository.findById(c.getIdClient()).get();
		client.setNom("TOTO");
		mockClientRepository.saveAndFlush(client);
		Client client2 = mockClientRepository.findById(c.getIdClient()).get();
		assertTrue("TOTO".equals(client2.getNom()));}
	
	
	@Test
	public void testRecupClientMail() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		
		mockClientRepository.save(c);
		Client client = mockClientRepository.findByMail(c.getMail()).get();
		assertTrue(c.getIdClient()==client.getIdClient());}
	
	
	
	@Test
	public void testRecuperationClientNomEtPrenom() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		Client c2 = new Client();
		c2.setNom("Meddah");
		c2.setPrenom("Mohamed");
		c2.setMail("mmeddah59@gmail.com");
		c2.setTelephone("0601010101");
		ArrayList<Client> listClientTest = new ArrayList<Client>();
		String nom = c.getNom();
		String prenom = c.getPrenom();
		mockClientRepository.save(c);
		mockClientRepository.save(c2);
		List <Client> listeClient = mockClientRepository.findByNomContainingAndPrenomContaining("Benjira","Mohammed");
		assertTrue(listeClient.size()==1);}
	
	@Test
	public void testSuppressionClientParID() {
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		Client c = new Client();
		c.setIdClient(0);
		c.setNom("Benjira");
		c.setPrenom("Mohammed");
		c.setMail("mbenjira@gmail.com");
		c.setTelephone("0606060606");
		Client c2 = new Client();
		c2.setIdClient(2);
		c2.setNom("Meddah");
		c2.setPrenom("Mohamed");
		c2.setMail("mmeddah59@gmail.com");
		c2.setTelephone("0601010101");
		mockClientRepository.save(c);
		mockClientRepository.save(c2);
		mockClientRepository.deleteById(0);	
		mockClientRepository.deleteById(2);
		List <Client> listeClient = mockClientRepository.findAll();
		assertTrue(listeClient.size()==0);}
	
	
	

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}
	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}
	@AfterEach
	public void tearDown() throws Exception {
	}
	
	

}
