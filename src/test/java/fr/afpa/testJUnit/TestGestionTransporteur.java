package fr.afpa.testJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Transporteur;
import fr.afpa.repository.dao.TransporteurRepository;


@RunWith(MockitoJUnitRunner.class)
public class TestGestionTransporteur {
	
	@Mock TransporteurRepository repo;
	
	@InjectMocks
	Transporteur trans;
	Authentification auth;
	

   

	@BeforeEach
	public void setUp() throws Exception {
		trans = new Transporteur();
		auth = new Authentification();
		trans.setNom("Benjira");
		trans.setPrenom("Mohammed");
		trans.setTelephone("0654387612");
		trans.setMail("mbenjira@gmail.com");
		auth.setLogin("benjira");
		auth.setPassword("mohammed");
		trans.setAuth(auth);	
		
	}


	@Test
	public void testCreateTransporteur() {
		TransporteurRepository mockTransRepo = RepositoryFactoryBuilder.builder().mock(TransporteurRepository.class);
		mockTransRepo.save(trans);
		assertTrue(!mockTransRepo.findAll().isEmpty());
	}
	
	@Test
	public void testFindByAuth() {
	TransporteurRepository mockTransRepo = RepositoryFactoryBuilder.builder().mock(TransporteurRepository.class);
	mockTransRepo.save(trans);
	assertEquals(trans,mockTransRepo.findByAuth(auth));
	}
	
	
	@Test
	public void testFindByMail() {
		TransporteurRepository mockTransRepo = RepositoryFactoryBuilder.builder().mock(TransporteurRepository.class);
		trans = new Transporteur();
		auth = new Authentification();
		trans.setNom("Benjira");
		trans.setPrenom("Mohammed");
		trans.setTelephone("0654387612");
		trans.setMail("mbenjira@gmail.com");
		auth.setLogin("benjira");
		auth.setPassword("mohammed");
		trans.setAuth(auth);	
		mockTransRepo.save(trans);
		assertEquals(trans,mockTransRepo.findByMail("mbenjira@gmail.com"));
	}



   




}