package fr.afpa.testJUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Authentification;
import fr.afpa.beans.Transporteur;
import fr.afpa.model.GestionConnexion;
import fr.afpa.repository.dao.ConnexionRepository;
import fr.afpa.repository.dao.TransporteurRepository;


@RunWith(MockitoJUnitRunner.class)
public class TestGestionAuthentification {
	
	@Mock ConnexionRepository repo;
	
	@InjectMocks
	Transporteur trans;
	Authentification auth;
	GestionConnexion gest;
	
	
	@BeforeEach
	public void setUp()  {
		trans = new Transporteur();
		auth = new Authentification();
		trans.setNom("Benjira");
		trans.setPrenom("Mohammed");
		trans.setTelephone("0654387612");
		trans.setMail("mbenjira@gmail.com");
		auth.setLogin("benjira");
		auth.setPassword("mohammed");
		trans.setAuth(auth);	
		
	}


	@Test
	public void testCheckAuth() {
		
		ConnexionRepository mockConnRepo = RepositoryFactoryBuilder.builder().mock(ConnexionRepository.class);	
		auth = new Authentification();
		auth.setLogin("benjira");
		auth.setPassword("mohammed");
		mockConnRepo.save(auth);
		assertEquals(auth,mockConnRepo.findByLoginAndPassword("benjira","mohammed"));
	}
	
	
	

}
