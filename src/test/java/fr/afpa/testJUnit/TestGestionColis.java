package fr.afpa.testJUnit;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.mmnaseri.utils.spring.data.dsl.factory.RepositoryFactoryBuilder;

import fr.afpa.beans.Client;
import fr.afpa.beans.Colis;
import fr.afpa.beans.Transporteur;
import fr.afpa.model.GestionClient;
import fr.afpa.model.GestionColis;
import fr.afpa.model.GestionTransporteur;
import fr.afpa.repository.dao.ClientRepository;
import fr.afpa.repository.dao.ColisRepository;
import fr.afpa.repository.dao.JournalDeBordRepository;
import fr.afpa.repository.dao.TransporteurRepository;



@RunWith(MockitoJUnitRunner.class)
public class TestGestionColis {
	@InjectMocks GestionColis gestionColis;
	@InjectMocks GestionClient gestionClient;
	@InjectMocks GestionTransporteur gestionTransporteur;
	@Mock ColisRepository colisRepo;	
	@Mock JournalDeBordRepository journalDeBordRepository;	
	@Mock ClientRepository clientRepo;	
	@Mock TransporteurRepository transRepo;	
	
//	
//	@BeforeEach
//	public void setUp() throws Exception {		
//	}
//	

	@Test
	public void testRecupColis() {
		JournalDeBordRepository mockJournalDeBordRepository = RepositoryFactoryBuilder.builder().mock(JournalDeBordRepository.class);
		ClientRepository mockClientRepository = RepositoryFactoryBuilder.builder().mock(ClientRepository.class);
		TransporteurRepository mockTransporteurRepository = RepositoryFactoryBuilder.builder().mock(TransporteurRepository.class);
		ColisRepository mockColisRepository = RepositoryFactoryBuilder.builder().mock(ColisRepository.class);
		Colis c = new Colis();		
		c.setIdColis(1);
		mockColisRepository.save(c);
		Colis colisRepo = mockColisRepository.findByIdColis(c.getIdColis());
		assertTrue(c.getIdColis()==colisRepo.getIdColis());}
//
//
//	@BeforeAll
//	public static void setUpBeforeClass() throws Exception {
//	}
//	@AfterAll
//	public static void tearDownAfterClass() throws Exception {
//	}
//	@AfterEach
//	public void tearDown() throws Exception {
//	}
//	
	

}
