Bienvenue dans l'application LivColis réalisée par Mohamed Meddah, Amal Farès, Adrien Maurer et Ibrahim EL Kadiri au sein de l'afpa (cda-20156).

Cette application est une application de gestion de colis afin de mettre à jour en temps réel le déplacement de nos colis.
Elle a pour vocation de permettre un suivi rapide et de faciliter la gestion des colis que chaque transporteur doit gérer.

Les pré-requis à l'utilisation de cette application sont :
- L'installation de la JDK / JRE
- La création d'une base de donnée sur PGAdmin nommée : "livColis"ainsi que 
  d'un compte superUser nommé : "livColis"
  avec comme mot de passe : "livColis".
- L'installation de TOMCAT à la derniere version.
- Ajouter le fichier war dans le dossier webApps de Tomcat.
- Ouvrir le fichier startup.bat dans le dossier bin de tomcat (Attention à ce que Eclipse soit bien éteint car il peut y avoir un conflit).
- Vérifier que le déploiement a bien été réalisé en mettant l'url suivant : http://localhost:8080/cda20156-libDiscount-EE/
ce qui vous permettra d'atterir à l'accueil du site.


Les utilisateurs lambda pourront :
- consulter l'état d'avancée de leur colis.
- Recevoir des mails à chaque changement d'étape de leur colis.

Le transporteur pourra :
1- Se créer un compte.
2- Se connecter.
3- Créer des Points Relais, les modifier, les supprimer.
4- Créer des clients, les modifier, les supprimer.
5- Créer des Colis, les modifier, les supprimer (Attention, la création de colis nécessite des clients et des points relais).
6- Passer les colis qu'il gère aux points relais suivantes.
7- Voir l'avancée des colis qu'il doit livrer.

Pour tout complément d'informations ou bugs à corriger:
 Merci de contacter l'un des administrateurs par email : 
- mmeddah59@gmail.com
- amal.fares@hotmail.fr
- adrienmaurer@hotmail.com
- ibrahim.elkadiri@yahoo.fr


Pour les phases de tests, il sera nécessaire de passer les attributs beans de int à Integer ainsi que les paramètres des méthodes associées aux id.


Futures mises à jours :
- Ajouts de fonctionnalités diverses.
- Améliorations graphiques.
- Améliorations de la sécurité.